/******************************************************************************
 * Copyright : � Schauenburg (Pty) Limited, 2016
 * All rights reserved.   No part of this publication may be reproduced, stored
 * in  a retrieval system,  or  transmitted  in  any  form  or  by  any  means,
 * electronic, mechanical, photocopying, recording  or otherwise,  without  the
 * prior written permission of Schauenburg (Pty) Limited.
 * Template Number: MODBUS_MANAGER-0001
 *****************************************************************************/
#ifdef __cplusplus
extern "C"{
#endif
/**************************************************************************//**
 * \file		MODBUS_MANAGER???.???
 * \brief	Give a short description of the primary function of this file here.
 *****************************************************************************/
#ifndef _MODBUS_MANAGER_C_
#define _MODBUS_MANAGER_C_
#define THIS_MODULE				MODULE_MODBUS_MANAGER

//#include "file_transfer_master.h"
/******************************************************************************
 * VERSION CONTROL HISTORY
 *****************************************************************************/
/*
 * $Log$
 */


/******************************************************************************
 * INCLUDE FILES
 *****************************************************************************/
//OPERATING SYSTEM INCLUDES
//#include "cmsis_os.h"

//UTILITIES INCLUDES
//#include "project_conf.h"
#include "SCHTypeDefs.h"
#include "serror.h"
#include <string.h>
//#include "module.h"
//#include "api_conf.h"

//APPLICATION SOFTWARE INCLUDES

//UPPER DRIVER LAYER INCLUDES
//#include "pod_scanner.h"
//MIDDLE DRIVER LAYER INCLUDES
#include "modbus_master_stack.h"
//HARDWARE LAYER INCLUDES
//#include "scpu_conf.h"

//PRIVATE + CONF HEADER FILE(S)
#include "modbus_manager.h"
//#include "modbus_manager_conf.h"


/******************************************************************************
 * MODULE CONSTANTS
 *****************************************************************************/
#define MY_CONSTANT				(30)u					///< Description for my constant


/******************************************************************************
 * MODULE MACROS
 *****************************************************************************/
//Definitions for modbus map array:
//Array dimensions:
#define POD_TYPE_COUNT 		3
#define EVENT_TYPE_COUNT 	5
#define ENTRY_COUNT 		4
//Entry indexes:
#define FUNCTION_CODE 		0
#define POD_ADDRESS 		1
#define REGISTER_COUNT 		2
#define HEAD_ADDRESS		3







/******************************************************************************
 * MODULE TYPE DEFINITIONS
 *****************************************************************************/
typedef U8					MODBUS_MANAGER_HandleT;			///< some description


/******************************************************************************
 * GLOBAL VARIABLES
 *****************************************************************************/
BOOL MODBUS_MANAGER_VariableB = FALSE;								///< some description


/******************************************************************************
 * MODULE VARIABLES
 *****************************************************************************/
LOCAL U8		modbus_managerCntU8;						///< some description
LOCAL MODBUS_MASTER_RX_TX_S RequestS;
LOCAL MODBUS_MANAGER_PodEventS *RequestQueueHeadPS;//start of queue
LOCAL CHAR * responsePC = NULL;
LOCAL U8 buffer[256];//just for testing
//SemaphoreHandle_t requestQueueSEM = NULL;


LOCAL U16 MODBUS_MANAGER_ModbusMap[POD_TYPE_COUNT][EVENT_TYPE_COUNT][4] =
		{
			//Event             Event          Event
			{{3,0x86,2,0x00},{3,0x80,8,0x02},{0x10,0xA410,0,0},{0x10,0xA411,0,0},{0x03,0xA411,0,0}},//POD TYPE
			{{1,2,3,4},{1,2,3,4},{1,2,3,4},{1,2,3,4},{1,2,3,4}},//POD TYPE
			{{1,2,3,4},{1,2,3,4},{1,2,3,4},{1,2,3,4},{1,2,3,4}}//POD TYPE
		}; ////Each Entry is stored as: {Function Code,POD Address,Register Address,Head Address}
           //Eg PodType 0 EventType 0 gives {3,0x86,2,0x00}

/******************************************************************************
 * LOCAL FUNCTIONS PROTOTYPES
 *****************************************************************************/
LOCAL BOOL modbus_manager_funcB(VOID);
LOCAL void MODBUS_MANAGER_PopRequestQueue();
LOCAL U8   MODBUS_MANAGER_GetQueueCount();
LOCAL void MODBUS_MANAGER_PopulateRxTxRequest(MODBUS_MASTER_RX_TX_S * RequestPS);
//LOCAL POD_SCANNER_PodResponseS ResponseS;




/******************************************************************************
 * GLOBAL FUNCTIONS : STANDARD FUNCTIONS
 *****************************************************************************/

/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR MODBUS_MANAGER_InitR(VOID)
{

	//create a semaphore that triggers when the queue is populated
//	requestQueueSEM = xSemaphoreCreateBinary();

	//start up the request queue
    RequestQueueHeadPS = malloc(sizeof(MODBUS_MANAGER_PodEventS));

	RequestQueueHeadPS->next = NULL;
	RequestQueueHeadPS->eventTypeU8 = 0;
	RequestQueueHeadPS->podModbusAddressU8 = 0;
	RequestQueueHeadPS->podTypeU8 = 0;
	RequestQueueHeadPS->txDataPU8 = NULL;
	RequestQueueHeadPS->txDataLengthU8 = 0;



	//init master stack

//	MODBUS_MASTER_STACK_InitR(UART_PORT_3_BAUD_RATE,UART_PORT_3,TIMER_3,3);


	return(SERROR_NONE);
}	//end MODBUS_MANAGER_InitR()


/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR MODBUS_MANAGER_ExitR(VOID)
{
	//something
	return(SERROR_NONE);
}	//end MODBUS_MANAGER_ExitR()


/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR MODBUS_MANAGER_SleepR(MODBUS_MANAGER_SLEEP_E mode)
{
	//something
	return(SERROR_NONE);
}	//end MODBUS_MANAGER_SleepR()


/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR MODBUS_MANAGER_WakeupR(VOID)
{
	//something
	return(SERROR_NONE);
}	//end MODBUS_MANAGER_WakeupR()


/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR MODBUS_MANAGER_SelftestR(VOID)
{
	#if PROJECT_COMPILE_SELFTEST
	//something
	#endif	//PROJECT_COMPILE_SELFTEST
	return(SERROR_NONE);
}	//end MODBUS_MANAGER_SelftestR()


/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR MODBUS_MANAGER_XmlR(CHAR* attribCP, CHAR* value_inCP, CHAR* value_outCP, U16 value_maxlenU16)
{
	return(SERROR_NONE);
}	//end MODBUS_MANAGER_XmlR()





/******************************************************************************
 * GLOBAL FUNCTIONS : ADDITIONAL FUNCTIONS
 *****************************************************************************/


void MODBUS_MANAGER_Service()
{
	//entry point after being called by freeRTOS
//	UBaseType_t uxHighWaterMark;
//	uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );

	while(1)
	{

		//started by a semaphore
        if (/*xSemaphoreTake(requestQueueSEM, 0xffff) ==*/ TRUE)
		{
			//populate request structure
			MODBUS_MANAGER_PopulateRxTxRequest(&RequestS);


            responsePC = malloc(RequestS.RegisterCountU16*sizeof(U16));//size of register is 16bit thus multiplied by 2
		    memset(responsePC,0,RequestS.RegisterCountU16*sizeof(U16));



		    if(MODBUS_MASTER_STACK_TxRxR(&RequestS, responsePC)!= SERROR_TIMEOUT)
		    {
//				ResponseS.headRegisterAddressU16 = MODBUS_MANAGER_ModbusMap[RequestQueueHeadPS->podTypeU8][RequestQueueHeadPS->eventTypeU8][HEAD_ADDRESS];
//				memset(ResponseS.responseU8,0,MODBUS_MANAGER_RESPONSE_DATA_SIZE);
//				memcpy(ResponseS.responseU8,responsePC,RequestS.RegisterCountU16*sizeof(U16));
//				vPortFree(responsePC);
//				ResponseS.responseLengthU8 = RequestS.RegisterCountU16*sizeof(U16);
//				ResponseS.podMobusAddressU8 = RequestQueueHeadPS->podModbusAddressU8;
				//check whether to send to pod scanner or file transfer master


				if (RequestS.AddressU16 >= 42000 && RequestS.FunctionCodeU8 == 16  )
				{
                    //FILE_TRANSFER_send(0, RequestS.SlaveAddressU8);

				}
				else
				{
//					POD_SCANNER_PushResponseQueue(&ResponseS);
				}



		    }
		    else
		    {
		    	//send back error message  in queue
		    }


		    //queue the response
			MODBUS_MANAGER_PopRequestQueue();
		}
	}
}



void MODBUS_MANAGER_PushRequestQueue(MODBUS_MANAGER_PodEventS * NewEventS)
{

	MODBUS_MANAGER_PodEventS * SeekerPS;

	SeekerPS = RequestQueueHeadPS;

    if (SeekerPS->podModbusAddressU8 != 0)//Check of first event in list is empty
	{
		while (SeekerPS->next != NULL)
		{
			SeekerPS = SeekerPS->next;
		}

        SeekerPS->next = malloc(sizeof(MODBUS_MANAGER_PodEventS));
		SeekerPS->next->eventTypeU8 = NewEventS->eventTypeU8;
		SeekerPS->next->podModbusAddressU8 = NewEventS->podModbusAddressU8;
		SeekerPS->next->podTypeU8 = NewEventS->podTypeU8;
		SeekerPS->next->txDataPU8 = NewEventS->txDataPU8;
		SeekerPS->next->txDataLengthU8 = NewEventS->txDataLengthU8;
		SeekerPS->next->next = NULL;
	}
	else//else insert the new event at the start of the list.
	{
		RequestQueueHeadPS->eventTypeU8 = NewEventS->eventTypeU8;
		RequestQueueHeadPS->podModbusAddressU8 = NewEventS->podModbusAddressU8;
		RequestQueueHeadPS->podTypeU8 = NewEventS->podTypeU8;
		RequestQueueHeadPS->txDataPU8 = NewEventS->txDataPU8;
		RequestQueueHeadPS->txDataLengthU8 = NewEventS->txDataLengthU8;


		RequestQueueHeadPS->next = NULL;
	}

//	xSemaphoreGive(requestQueueSEM);//release the semaphore to start the Modbus manager service.
}






/******************************************************************************
 * See header file for details
 *****************************************************************************/
U8 MODBUS_MANAGER_AGlobalFuncU8(CHAR arg1C)
{
	return(1);
}	//end MODBUS_MANAGER_GlobalFuncU8()


/******************************************************************************
 * LOCAL FUNCTIONS
 *****************************************************************************/

/******************************************************************************
 * \brief
 * This is the brief description for the function.
 *
 * \param		none
 * \return							Some description of what the return value means.
 * \retval		SERROR_NONE		no errors occurred
 * \retval		SERROR_?			one or more errors
 *
 * \todo
 * Some future functionality.
 *****************************************************************************/
LOCAL SERROR local_funcB(VOID)
{
	return(FALSE);
}	//end local_funcB()

void MODBUS_MANAGER_PopulateRxTxRequest(MODBUS_MASTER_RX_TX_S * RequestPS)
{
	RequestPS->SlaveAddressU8=RequestQueueHeadPS->podModbusAddressU8;
    RequestPS->FunctionCodeU8 = (U8)MODBUS_MANAGER_ModbusMap[RequestQueueHeadPS->podTypeU8][RequestQueueHeadPS->eventTypeU8][FUNCTION_CODE];
	RequestPS->AddressU16 = MODBUS_MANAGER_ModbusMap[RequestQueueHeadPS->podTypeU8][RequestQueueHeadPS->eventTypeU8][POD_ADDRESS];
	RequestPS->txDataPU8 = RequestQueueHeadPS->txDataPU8;
	RequestPS->byteCountU8 = RequestQueueHeadPS->txDataLengthU8;

	//if data is being sent the register count depends on the data length. Otherwise it's derived from the modbus map.
	if (RequestQueueHeadPS->txDataLengthU8 == 0)
	{
		RequestPS->RegisterCountU16 = MODBUS_MANAGER_ModbusMap[RequestQueueHeadPS->podTypeU8][RequestQueueHeadPS->eventTypeU8][REGISTER_COUNT];
	}
	else
	{
		if (RequestQueueHeadPS->txDataLengthU8%2 == 0 )
		{
			RequestPS->RegisterCountU16 = RequestQueueHeadPS->txDataLengthU8/2;
		}
		else
		{
			RequestPS->RegisterCountU16 = RequestQueueHeadPS->txDataLengthU8/2 + 1;
		}
	}


}

void MODBUS_MANAGER_PopRequestQueue()
{

	if (RequestQueueHeadPS->next != NULL)
	{	//remove the request from the queue
		RequestQueueHeadPS = RequestQueueHeadPS->next; //remove the first entry from the Request Queue
	}
	else
	{	//otherwise just clear the last remaining request in the queue
        RequestQueueHeadPS->eventTypeU8 = 0;
        RequestQueueHeadPS->podModbusAddressU8 = 0;
        RequestQueueHeadPS->podTypeU8 = 0;
	}

	if (MODBUS_MANAGER_GetQueueCount()>=1)//if there is still something left in the queue
	{
//		xSemaphoreGive(requestQueueSEM);//release the semaphore to start the Modbus manager service.
	}
}

U8 MODBUS_MANAGER_GetQueueCount()
{
	U8 CountU8 = 0;
	MODBUS_MANAGER_PodEventS * SeekerPS;

	SeekerPS = RequestQueueHeadPS;

    if (RequestQueueHeadPS->podModbusAddressU8 != 0)
	{
		CountU8++;
	}


	while (SeekerPS->next != NULL)
	{
		SeekerPS = SeekerPS->next;
		CountU8++;
	}

	return CountU8;

}

#endif	//#ifndef _MODBUS_MANAGER_C_
#ifdef __cplusplus
}
#endif
/******************************************************************************
 * END
 *****************************************************************************/
