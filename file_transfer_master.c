/******************************************************************************
 * Copyright : � Schauenburg (Pty) Limited, 2016
 * All rights reserved.   No part of this publication may be reproduced, stored
 * in  a retrieval system,  or  transmitted  in  any  form  or  by  any  means,
 * electronic, mechanical, photocopying, recording  or otherwise,  without  the
 * prior written permission of Schauenburg (Pty) Limited.
 * FILE_TRANSFER_MASTER Number: FILE_TRANSFER_MASTER-0001
 *****************************************************************************/

/**************************************************************************//**
 * \file		FILE_TRANSFER_MASTER???.???
 * \brief	Give a short description of the primary function of this file here.
 *****************************************************************************/

#ifdef __cplusplus
extern "C"{
#endif

#ifndef _FILE_TRANSFER_MASTER_C_
#define _FILE_TRANSFER_MASTER_C_
#define THIS_MODULE				MODULE_FILE_TRANSFER_MASTER


/******************************************************************************
 * VERSION CONTROL HISTORY
 *****************************************************************************/
/*
 * $Log$
 */


/******************************************************************************
 * INCLUDE FILES
 *****************************************************************************/
//OPERATING SYSTEM INCLUDES


//UTILITIES INCLUDES

//APPLICATION SOFTWARE INCLUDES

//UPPER DRIVER LAYER INCLUDES

//MIDDLE DRIVER LAYER INCLUDES
#include <modbus_master_stack.h>
//HARDWARE LAYER INCLUDES


//PRIVATE + CONF HEADER FILE(S)
#include "file_transfer_master.h"
#include "widget.h"



/******************************************************************************
 * MODULE CONSTANTS
 *****************************************************************************/


/******************************************************************************
 * MODULE MACROS
 *****************************************************************************/
#define FILE_TRANSFER_MASTER_PACKET_SIZE        220
/******************************************************************************
 * MODULE TYPE DEFINITIONS
 *****************************************************************************/
typedef struct sessionT
{
	U8 sessionIDU8;
    U16 transferNumberU16;
	CHAR* fileNamePC;
	U32 fileSizeU32;
	U32 fileRemainingU32;
    FILE * fileP;
    void * master;
}sessionS;

/******************************************************************************
 * GLOBAL VARIABLES
 *****************************************************************************/


/******************************************************************************
 * MODULE VARIABLES
 *****************************************************************************/
LOCAL sessionS sessionTableAS[5] = {{0,0,NULL,0,0,NULL,NULL},{0,0,NULL,0,0,NULL,NULL},{0,0,NULL,0,0,NULL,NULL},{0,0,NULL,0,0,NULL,NULL},{0,0,NULL,0,0,NULL,NULL}};
LOCAL U8 txBufferAU8[256];


/******************************************************************************
 * LOCAL FUNCTIONS PROTOTYPES
 *****************************************************************************/


/******************************************************************************
 * GLOBAL FUNCTIONS : STANDARD FUNCTIONS
 *****************************************************************************/


/******************************************************************************
 * See header file for details
 *****************************************************************************/

/******************************************************************************
 * See header file for details
 *****************************************************************************/


/******************************************************************************
 * See header file for details
 *****************************************************************************/


/******************************************************************************
 * See header file for details
 *****************************************************************************/


/******************************************************************************
 * See header file for details
 *****************************************************************************/


/******************************************************************************
 * See header file for details
 *****************************************************************************/


/******************************************************************************
 * GLOBAL FUNCTIONS : ADDITIONAL FUNCTIONS
 *****************************************************************************/
LOCAL fileXferDefinitionS transferDefinitionS;
MODBUS_MASTER_RX_TX_S requestStartS;

SERROR FILE_TRANSFER_start(CHAR* fileNamePC,U8 podModbusAdressU8,U8 fileTypeU8,void * master)
		{

			//find file name
			//populate fileXferDefinition


           sessionTableAS[podModbusAdressU8-1].fileP = fopen(fileNamePC,"rb");

           fseek(sessionTableAS[podModbusAdressU8-1].fileP,0L,SEEK_END);
           transferDefinitionS.lengthU32 = ftell(sessionTableAS[podModbusAdressU8-1].fileP);
           fseek(sessionTableAS[podModbusAdressU8-1].fileP,0L,SEEK_SET);





            transferDefinitionS.packet_sizeU8 = FILE_TRANSFER_MASTER_PACKET_SIZE;
			transferDefinitionS.last_packet_sizeU8 = transferDefinitionS.lengthU32%transferDefinitionS.packet_sizeU8;
            transferDefinitionS.packet_countU16 = (U16)(transferDefinitionS.lengthU32/transferDefinitionS.packet_sizeU8);





			if (transferDefinitionS.last_packet_sizeU8 > 0)
			{
				transferDefinitionS.packet_countU16++;
			}

			transferDefinitionS.master_session_idU8 = 1; //should we hardcode and limit to 1?
			transferDefinitionS.timeoutU8 = 0xFF; //find out in what unit this is
			transferDefinitionS.file_versionU32 = 1;//clarify?
			transferDefinitionS.crcU32 = 0xAAAAAAAA; //not done yet talk to Jaco
            transferDefinitionS.typeU16 = fileTypeU8; //0x4343;//clarify?


			//setup transfer


            requestStartS.SlaveAddressU8 = podModbusAdressU8;
            requestStartS.FunctionCodeU8 = 0x10 ;
            requestStartS.AddressU16 = 0xA410  ;
            requestStartS.byteCountU8 = (U8)sizeof(transferDefinitionS) ;

            if (requestStartS.byteCountU8%2 == 0 )
            {
                requestStartS.RegisterCountU16 = requestStartS.byteCountU8/2;
            }
            else
            {
                requestStartS.RegisterCountU16 = requestStartS.byteCountU8/2 + 1;
            }


            requestStartS.txDataPU8 = (U8*)&transferDefinitionS;

//			requestStartS.eventTypeU8 = 2; //where should we get the event type from?
//			requestStartS.podModbusAddressU8 = podModbusAdressU8;
//			requestStartS.podTypeU8 = 0; //all pod types will have this command available
//			requestStartS.txDataPU8 = (U8*)&transferDefinitionS;
//			requestStartS.txDataLengthU8 = (U8)sizeof(transferDefinitionS);


			sessionTableAS[podModbusAdressU8-1].sessionIDU8 = transferDefinitionS.master_session_idU8;
            sessionTableAS[podModbusAdressU8-1].transferNumberU16 = 0;
			sessionTableAS[podModbusAdressU8-1].fileNamePC = fileNamePC;
			sessionTableAS[podModbusAdressU8-1].fileSizeU32 = transferDefinitionS.lengthU32;
			sessionTableAS[podModbusAdressU8-1].fileRemainingU32 = transferDefinitionS.lengthU32;
            sessionTableAS[podModbusAdressU8-1].master = master;



            CHAR  response[100];

            memset(response,0,sizeof(response));

             MODBUS_MASTER_STACK_TxRxR(&requestStartS,response);

             FILE_TRANSFER_send(1, 1);
            //MODBUS_MANAGER_PushRequestQueue(&requestStartS);
			//read from simple file

			return SERROR_NONE;
		}

LOCAL fileXferPayloadS payloadS;
LOCAL MODBUS_MASTER_RX_TX_S requestSendS;
SERROR FILE_TRANSFER_send(U8 sessionIdU8, U8 podModbusAddressU8)
{
    //update progress bar

    float progress =  100*((float)sessionTableAS[podModbusAddressU8-1].fileSizeU32 - (float)sessionTableAS[podModbusAddressU8-1].fileRemainingU32)/(float)sessionTableAS[podModbusAddressU8-1].fileSizeU32;

    updateFileProgressWrapper(sessionTableAS[podModbusAddressU8-1].master,(U8)progress);

    delayWrapper(sessionTableAS[podModbusAddressU8-1].master,1);//update the gui


	if (sessionTableAS[podModbusAddressU8-1].fileRemainingU32 <= 0)
	{
        FILE_TRANSFER_getSessionInfo(sessionIdU8,1);
		return SERROR_NONE;
      }

     int scope =   sessionTableAS[podModbusAddressU8-1].fileRemainingU32;

	U8* bufferPC = NULL;
	U32 fileOffsetU32 = sessionTableAS[podModbusAddressU8-1].fileSizeU32 - sessionTableAS[podModbusAddressU8-1].fileRemainingU32;
    U8 readLengthU8 = 0;

    if (sessionTableAS[podModbusAddressU8-1].fileRemainingU32 > FILE_TRANSFER_MASTER_PACKET_SIZE)
	{
        readLengthU8 = FILE_TRANSFER_MASTER_PACKET_SIZE;
	}
	else
	{
        readLengthU8 = (U8)sessionTableAS[podModbusAddressU8-1].fileRemainingU32;
	}



	memset(txBufferAU8,0,sizeof(txBufferAU8));

	payloadS.masterSessionIdU8 = sessionTableAS[podModbusAddressU8-1].sessionIDU8;
    payloadS.lengthU8 = readLengthU8;
    payloadS.packetNoU16 = sessionTableAS[podModbusAddressU8-1].transferNumberU16;

    if (payloadS.packetNoU16 == 0xd5)
    {
        printf("hiers die probleem");
    }


	memcpy(txBufferAU8,&payloadS,sizeof(payloadS));

    fread(txBufferAU8+sizeof(payloadS),sizeof(U8),readLengthU8,sessionTableAS[podModbusAddressU8-1].fileP);



    //FILE_readUINT(sessionTableAS[podModbusAddressU8-1].fileNamePC, fileOffsetU32,readLengthU32,txBufferAU8+sizeof(payloadS));

	//setup transfer




//	MODBUS_MANAGER_PodEventS requestS;

    requestSendS.SlaveAddressU8     = podModbusAddressU8;
    requestSendS.FunctionCodeU8     = 0x10;
    requestSendS.AddressU16         = 0xA411;
    requestSendS.byteCountU8        = sizeof(payloadS) + (readLengthU8);

    if (requestSendS.byteCountU8%2 == 0 )
    {
        requestSendS.RegisterCountU16 = requestSendS.byteCountU8/2;
    }
    else
    {
        requestSendS.RegisterCountU16 = requestSendS.byteCountU8/2 + 1;
        requestSendS.byteCountU8 = requestSendS.byteCountU8 + 1;
    }


    requestSendS.txDataPU8 = (U8*) txBufferAU8;

//	requestSendS.eventTypeU8 = 3;
//	requestSendS.next = NULL;
//	requestSendS.podModbusAddressU8 = podModbusAddressU8;
//	requestSendS.podTypeU8 = 0; //all pod types will have this command available
//	requestSendS.txDataLengthU8 = sizeof(payloadS) + readLengthU32;
//	requestSendS.txDataPU8 =(U8*) txBufferAU8;
//	MODBUS_MANAGER_PushRequestQueue(&requestSendS);

    CHAR  response[100];
    memset(response,0,sizeof(response));

    U16 requestedTransferNumberU16 = 0;

    BOOL nextB = FALSE;
    U8 retryCountU8 = 0;

    while (!nextB && retryCountU8 < 3 )
    {
        MODBUS_MASTER_STACK_TxRxR(&requestSendS,response);

        U8  high = *(response+2);
        U8  low = *(response+3);

        requestedTransferNumberU16 = (high<<8)+ low;

        if (requestedTransferNumberU16 == (sessionTableAS[podModbusAddressU8-1].transferNumberU16+1))
        {
            nextB = TRUE;
        }
        else
        {
            retryCountU8++;
        }

    }

    if (retryCountU8 >=3)
    {
        return SERROR_TIMEOUT;
    }

    sessionTableAS[podModbusAddressU8-1].fileRemainingU32 = sessionTableAS[podModbusAddressU8-1].fileRemainingU32 - readLengthU8;
    sessionTableAS[podModbusAddressU8-1].transferNumberU16++;

    FILE_TRANSFER_send(1, 1);



	return SERROR_NONE;
}


LOCAL sessionInfoS sessionStateS;
LOCAL MODBUS_MASTER_RX_TX_S requestSessionStateS;
SERROR FILE_TRANSFER_getSessionInfo(U8 sessionIdU8, U8 podModbusAddressU8)
{

    requestSessionStateS.SlaveAddressU8 = podModbusAddressU8;
    requestSessionStateS.FunctionCodeU8 = 0x03;
    requestSessionStateS.AddressU16 = 0xA474 + sessionIdU8;
    requestSessionStateS.byteCountU8 = 0;
    requestSessionStateS.txDataPU8 = NULL;

            int test = sizeof(sessionStateS);
            if (sizeof(sessionStateS)%2 == 0 )
            {
                requestSessionStateS.RegisterCountU16 = sizeof(sessionStateS)/2;
            }
            else
            {
                requestSessionStateS.RegisterCountU16 = sizeof(sessionStateS)/2 + 1;
            }

            CHAR response[100];

            MODBUS_MASTER_STACK_TxRxR(&requestSessionStateS,response);


//	requestSessionStateS.eventTypeU8 = 4;
//	requestSessionStateS.next = NULL;
//	requestSessionStateS.podModbusAddressU8 = podModbusAddressU8;
//	requestSessionStateS.podTypeU8 = 0;
//	requestSessionStateS.txDataLengthU8 = 0;
//	requestSessionStateS.txDataPU8 = NULL;

	return SERROR_NONE;
}


//SERROR FILE_TRANSFER_start(CHAR* fileNamePC,U8 podModbusAdressU8)

/******************************************************************************
 * See header file for details
 *****************************************************************************/


/******************************************************************************
 * LOCAL FUNCTIONS
 *****************************************************************************/

/******************************************************************************
 * \brief
 * This is the brief description for the function.
 *
 * \param		none
 * \return							Some description of what the return value means.
 * \retval		SERROR_NONE		no errors occurred
 * \retval		SERROR_?			one or more errors
 *
 * \todo
 * Some future functionality.
 *****************************************************************************/


#endif	//#ifndef _FILE_TRANSFER_MASTER_C_

#ifdef __cplusplus
}
#endif

/******************************************************************************
 * END
 *****************************************************************************/
