/******************************************************************************
 * Copyright : � Schauenburg Systems (Pty) Limited, 2011
 * All rights reserved.   No part of this publication may be reproduced, stored
 * in  a retrieval system,  or  transmitted  in  any  form  or  by  any  means,
 * electronic, mechanical, photocopying, recording  or otherwise,  without  the
 * prior written permission of Schauenburg Systems (Pty) Limited.
 * Template Number: MODBUS_MASTER_STACK-0002
 *****************************************************************************/
#ifdef __cplusplus
extern "C"{
#endif
/**************************************************************************//**
 * \file		MODBUS_MASTER_STACK???.???
 * \brief	Give a short description of the primary function of this file here.
 *****************************************************************************/
#ifndef _MODBUS_MASTER_STACK_H_
#define _MODBUS_MASTER_STACK_H_


/******************************************************************************
 * VERSION CONTROL HISTORY
 *****************************************************************************/
/*
 * $Log$
 */


/******************************************************************************
 * INCLUDE FILES
 *****************************************************************************/
//OPERATING SYSTEM INCLUDES
//#include "cmsis_os.h"
//UTILITIES INCLUDES
//#include "project_conf.h"
#include "SCHTypeDefs.h"
#include "serror.h"

//APPLICATION SOFTWARE INCLUDES

//UPPER DRIVER LAYER INCLUDES

//MIDDLE DRIVER LAYER INCLUDES

//HARDWARE LAYER INCLUDES


/******************************************************************************
 * GLOBAL CONSTANTS
 *****************************************************************************/


/******************************************************************************
 * GLOBAL MACROS
 *****************************************************************************/
#define MODBUS_MASTER_STACK_MACRO(_x,_y)		(_x + _y)


/******************************************************************************
 * GLOBAL TYPE DEFINITIONS
 *****************************************************************************/

/**
 * Defines the specific sleep modes for this module.
 */
typedef enum MODBUS_MASTER_STACK_SLEEP_E
{
	MODBUS_MASTER_STACK_SLEEP_SLOW,			///< some description
	MODBUS_MASTER_STACK_SLEEP_1,				///< some description
	MODBUS_MASTER_STACK_SLEEP_2,				///< some description
	MODBUS_MASTER_STACK_SLEEP_FULL,			///< some description
} MODBUS_MASTER_STACK_SLEEP_E;

/**
 * This structure represent some entity.
 */
typedef struct MODBUS_MASTER_STACK_TYPE_T
{
	F32		ValF32;			///< floating-point value
	U32		ValU32;			///< unsigned int-32 value
} MODBUS_MASTER_STACK_TYPE_S;

typedef struct MODBUS_MASTER_RX_TX_T
{
	U8 SlaveAddressU8;
	U8 FunctionCodeU8;
	U16 AddressU16;
	U16 RegisterCountU16;
	U8 byteCountU8;
	U8 * txDataPU8;

}MODBUS_MASTER_RX_TX_S;



/******************************************************************************
 * GLOBAL VARIABLE DECLARATIONS
 *****************************************************************************/
GLOBAL BOOL MODBUS_MASTER_STACK_VariableB;
GLOBAL BOOL MODBUS_MASTER_STACK_SilentTimeElapsedB;
GLOBAL BOOL MODBUS_MASTER_STACK_AwaitingResponseB;



/******************************************************************************
 * GLOBAL FUNCTIONS : STANDARD FUNCTIONS
 *****************************************************************************/
GLOBAL SERROR MODBUS_MASTER_STACK_InitR(void * rs485Thread);

GLOBAL SERROR MODBUS_MASTER_STACK_TxRxR(MODBUS_MASTER_RX_TX_S * RequestS,CHAR * RxDataPC);


/******************************************************************************
 * GLOBAL FUNCTIONS : ADDITIONAL FUNCTIONS
 *****************************************************************************/

/*****************************************************************************
 * \brief
 * Provides the XML interface for this module.
 * 
 * \param		attrib			attribute string
 * \param		value_in			value-in string
 * \param		value_out		value-out string
 * \param		value_maxlen	max length for value-out string
 * \return		none				
 *****************************************************************************/
//GLOBAL U8 MODBUS_MASTER_STACK_AGlobalFuncU8(CHAR arg1C);


#endif	//end #ifndef _MODBUS_MASTER_STACK_H_
#ifdef __cplusplus
}
#endif
/******************************************************************************
 * END
 *****************************************************************************/
