/******************************************************************************
 * Copyright : � Schauenburg Systems (Pty) Limited, 2011
 * All rights reserved.   No part of this publication may be reproduced, stored
 * in  a retrieval system,  or  transmitted  in  any  form  or  by  any  means,
 * electronic, mechanical, photocopying, recording  or otherwise,  without  the
 * prior written permission of Schauenburg Systems (Pty) Limited.
 * Template Number: MODBUS_MANAGER-0002
 *****************************************************************************/
#ifdef __cplusplus
extern "C"{
#endif
/**************************************************************************//**
 * \file		MODBUS_MANAGER???.???
 * \brief	Give a short description of the primary function of this file here.
 *****************************************************************************/
#ifndef _MODBUS_MANAGER_H_
#define _MODBUS_MANAGER_H_


/******************************************************************************
 * VERSION CONTROL HISTORY
 *****************************************************************************/
/*
 * $Log$
 */


/******************************************************************************
 * INCLUDE FILES
 *****************************************************************************/
//OPERATING SYSTEM INCLUDES

//UTILITIES INCLUDES
//#include "project_conf.h"
#include "SCHTypeDefs.h"
#include "serror.h"

//APPLICATION SOFTWARE INCLUDES

//UPPER DRIVER LAYER INCLUDES

//MIDDLE DRIVER LAYER INCLUDES

//HARDWARE LAYER INCLUDES


/******************************************************************************
 * GLOBAL CONSTANTS
 *****************************************************************************/


/******************************************************************************
 * GLOBAL MACROS
 *****************************************************************************/
#define MODBUS_MANAGER_MACRO(_x,_y)		(_x + _y)

#define MODBUS_MANAGER_RESPONSE_DATA_SIZE 			251


/******************************************************************************
 * GLOBAL TYPE DEFINITIONS
 *****************************************************************************/
typedef struct MODBUS_MANAGER_PodEventT
{
	U8 eventTypeU8;
	U8 podTypeU8;
	U8 podModbusAddressU8;
	U8 * txDataPU8;
	U8 txDataLengthU8;
	struct MODBUS_MANAGER_PodEventT * next;
}MODBUS_MANAGER_PodEventS;


/**
 * Defines the specific sleep modes for this module.
 */
typedef enum MODBUS_MANAGER_SLEEP_E
{
	MODBUS_MANAGER_SLEEP_SLOW,			///< some description
	MODBUS_MANAGER_SLEEP_1,				///< some description
	MODBUS_MANAGER_SLEEP_2,				///< some description
	MODBUS_MANAGER_SLEEP_FULL,			///< some description
} MODBUS_MANAGER_SLEEP_E;

/**
 * This structure represent some entity.
 */
typedef struct MODBUS_MANAGER_TYPE_T
{
	F32		ValF32;			///< floating-point value
	U32		ValU32;			///< unsigned int-32 value
} MODBUS_MANAGER_TYPE_S;


/******************************************************************************
 * GLOBAL VARIABLE DECLARATIONS
 *****************************************************************************/
GLOBAL BOOL MODBUS_MANAGER_VariableB;




/******************************************************************************
 * GLOBAL FUNCTIONS : STANDARD FUNCTIONS
 *****************************************************************************/
GLOBAL SERROR MODBUS_MANAGER_InitR(VOID);
GLOBAL SERROR MODBUS_MANAGER_ExitR(VOID);
GLOBAL SERROR MODBUS_MANAGER_SleepR(MODBUS_MANAGER_SLEEP_E mode);
GLOBAL SERROR MODBUS_MANAGER_WakeupR(VOID);
GLOBAL SERROR MODBUS_MANAGER_SelftestR(VOID);
GLOBAL SERROR MODBUS_MANAGER_XmlR(CHAR* attribCP, CHAR* value_inCP, CHAR* value_outCP, U16 value_maxlenU16);


/******************************************************************************
 * GLOBAL FUNCTIONS : ADDITIONAL FUNCTIONS
 *****************************************************************************/
GLOBAL void MODBUS_MANAGER_Service();
GLOBAL void MODBUS_MANAGER_PushRequestQueue(MODBUS_MANAGER_PodEventS *EventS);


/*****************************************************************************
 * \brief
 * Provides the XML interface for this module.
 * 
 * \param		attrib			attribute string
 * \param		value_in			value-in string
 * \param		value_out		value-out string
 * \param		value_maxlen	max length for value-out string
 * \return		none				
 *****************************************************************************/
GLOBAL U8 MODBUS_MANAGER_AGlobalFuncU8(CHAR arg1C);


#endif	//end #ifndef _MODBUS_MANAGER_H_
#ifdef __cplusplus
}
#endif
/******************************************************************************
 * END
 *****************************************************************************/
