/******************************************************************************
 * Copyright : � Schauenburg System (Pty) Limited, 2015
 * All rights reserved.   No part of this publication may be reproduced, stored
 * in  a retrieval system,  or  transmitted  in  any  form  or  by  any  means,
 * electronic, mechanical, photocopying, recording  or otherwise,  without  the
 * prior written permission of Schauenburg System (Pty) Limited.
 * JVRDesign Template Number: TEMPLATE-0002
 *****************************************************************************/

/**************************************************************************//**
 * \file		error.h.h
 * \created		8:22:03 PM 03 Jun 2015
 * \author		Daniel Janse Van Rensburg
 * \brief	Header file for MODULE module.
 *****************************************************************************/
#ifdef __cplusplus
extern "C"{
#endif

#ifndef _error_H_
#define _error_H_


/******************************************************************************
 * VERSION CONTROL HISTORY
 *****************************************************************************/
/*
 * $Log$
 */


/******************************************************************************
 * INCLUDE FILES
 *****************************************************************************/
//OPERATING SYSTEM INCLUDES

//UTILITIES INCLUDES

//APPLICATION SOFTWARE INCLUDES

//UPPER DRIVER LAYER INCLUDES

//MIDDLE DRIVER LAYER INCLUDES

//HARDWARE LAYER INCLUDES


/******************************************************************************
 * GLOBAL CONSTANTS
 *****************************************************************************/
///Category 1  :  zero   :  no error
#define SERROR_CAT1_START				0
#define SERROR_NONE						0

///Category 2  :  1-20   :  generic errors for all modules
#define SERROR_CAT2_START				1
#define SERROR_ALL						1			///< A generic code to indicate that an error happened.
#define SERROR_NOMEM						2			///< Cannot allocate memory (from heap)
#define SERROR_REENTRY					3			///< function is not re-entrant and already called from another task
#define SERROR_UNINIT					4			///< The specific module has not been initialised yet
#define SERROR_CHILDINIT				    5			///< A child-init of the current init function failed
#define SERROR_BADPARAM					6			///< one or more arguments is not what they should be
#define SERROR_TIMEOUT					7			///< A timeout of some sort occurred
#define SERROR_INVALIDBRANCH			    8			///< When a branch of code is executed that should not be executed.
#define SERROR_NOEXECUTION				9			///< The function or branch wasn't executed
#define SERROR_BADCMD					10			///< XML command not recognised
#define SERROR_BADFILENAME				11			///< A bad filename was provided
#define SERROR_BADCRC					12			///< Incorrect CRC encountered
#define SERROR_AWAKEN					13			///< Used to indicate an awakening
#define SERROR_BUFFER_EMPTY				14			///< Used to indicate an empty buffer
#define SERROR_NOT_ENOUGH_DATA			15			// Not enough data to proceed


/**
 * Category 3  :  21-255 :  module-specific error codes declared in the
 * module's header file.
 * To use this define an enumerated typedef in the specific header file where
 * the first constant in the enum is set equal to SERROR_CAT3_START.
 */
#define SERROR_CAT3_START				30


#define SERROR_PIO_START				50

#define SERROR_I2C_START				60
#define SERROR_I2C_INVALID_STATE		61

/******************************************************************************
 * GLOBAL MACROS
 *****************************************************************************/


/******************************************************************************
 * GLOBAL TYPE DEFINITIONS
 *****************************************************************************/
typedef unsigned char		SERROR;				// 8-bit unsigned
//typedef U8		SERROR;				// 8-bit unsigned

/******************************************************************************
 * GLOBAL VARIABLE DECLARATIONS
 *****************************************************************************/


/******************************************************************************
 * GLOBAL FUNCTIONS : STANDARD FUNCTIONS
 *****************************************************************************/


/******************************************************************************
 * GLOBAL FUNCTIONS : ADDITIONAL FUNCTIONS
 *****************************************************************************/

/******************************************************************************
 * \brief
 * Handles incoming MODULE XMODEM data:
 * 	- store a header
 * 	- stores the data
 * 	- sends both as alarms
 *
 * \param		DataPU8 - Source data to store and send
 * \param		LengthU16 - Length of the data
 * 
 * \return		SERROR
 * \retval		SERROR_NONE - No processing errors
 * \retval		SERROR_ALL - ...
 *****************************************************************************/




#endif	// _error_H_

#ifdef __cplusplus
}
#endif
/******************************************************************************
 * END
 *****************************************************************************/
