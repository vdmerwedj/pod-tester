#-------------------------------------------------
#
# Project created by QtCreator 2017-08-29T11:10:56
#
#-------------------------------------------------



VERSION = 1.0.0.0
DEFINES += APP_VERSION=\\\"$$VERSION\\\"
QT       += core gui serialport charts


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pod_tester
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        widget.cpp \
    pod_detector.c \
    crc.c \
    mbcrc.c \
    modbus_manager.c \
    modbus_master_stack.c \
    file_transfer_master.c

HEADERS += \
        widget.h \
    pod_detector.h \
    crc.h \
    SCHTypeDefs.h \
    serror.h \
    mbcrc.h \
    modbus_manager.h \
    modbus_master_stack.h \
    file_transfer_master.h

FORMS += \
        widget.ui
