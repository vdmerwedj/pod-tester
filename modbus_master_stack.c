/******************************************************************************
 * Copyright : � Schauenburg (Pty) Limited, 2016
 * All rights reserved.   No part of this publication may be reproduced, stored
 * in  a retrieval system,  or  transmitted  in  any  form  or  by  any  means,
 * electronic, mechanical, photocopying, recording  or otherwise,  without  the
 * prior written permission of Schauenburg (Pty) Limited.
 * Template Number: MODBUS_MASTER_STACK-0001
 *****************************************************************************/
#ifdef __cplusplus
extern "C"{
#endif
/**************************************************************************//**
 * \file		MODBUS_MASTER_STACK???.???
 * \brief	Give a short description of the primary function of this file here.
 *****************************************************************************/
#ifndef _MODBUS_MASTER_STACK_C_
#define _MODBUS_MASTER_STACK_C_
#define THIS_MODULE				MODULE_MODBUS_MASTER_STACK


/******************************************************************************
 * VERSION CONTROL HISTORY
 *****************************************************************************/
/*
 * $Log$
 */


/******************************************************************************
 * INCLUDE FILES
 *****************************************************************************/
//OPERATING SYSTEM INCLUDES
//#include "Schauenburg_os.h"
//#include <Windows.h>

////UTILITIES INCLUDES
//#include "project_conf.h"
//#include "typedef.h"
#include "serror.h"
//#include "module.h"
#include "mbcrc.h"
#include <stdlib.h>
#include <string.h>
#include<stdio.h>



////APPLICATION SOFTWARE INCLUDES
//
////UPPER DRIVER LAYER INCLUDES
//
////MIDDLE DRIVER LAYER INCLUDES
//
////HARDWARE LAYER INCLUDES
//#include "api_conf.h"
//#include "stm32f4xx_hal.h"
//#include "scpu_conf.h"
//
////PRIVATE + CONF HEADER FILE(S)
#include "modbus_master_stack.h"
//#include "template_conf.h"
//#include "file_transfer_master.h"
#include "widget.h"


/******************************************************************************
 * MODULE CONSTANTS
 *****************************************************************************/
#define MY_CONSTANT				(30)u					///< Description for my constant
#define HEADERSIZE				6u


/******************************************************************************
 * MODULE MACROS
 *****************************************************************************/


/******************************************************************************
 * MODULE TYPE DEFINITIONS
 *****************************************************************************/
typedef U8					MODBUS_MASTER_STACK_HandleT;			///< some description


/******************************************************************************
 * GLOBAL VARIABLES
 *****************************************************************************/
BOOL MODBUS_MASTER_STACK_VariableB = FALSE;								///< some description
BOOL MODBUS_MASTER_STACK_SilentTimeElapsedB = FALSE;
//BOOL MODBUS_MASTER_STACK_AwaitingResponseB = 0;


/******************************************************************************
 * MODULE VARIABLES
 *****************************************************************************/
LOCAL U32		templateCntU32;						///< some description
LOCAL U32 		ModbusBaudRateU32;
LOCAL U32 		InterMessageIdleTimeU32;
LOCAL void * 		comportThread;
LOCAL U8 		ModbusMasterTimerU8;
LOCAL U8 		AttemptLimitU8;





/******************************************************************************
 * LOCAL FUNCTIONS PROTOTYPES
 *****************************************************************************/

LOCAL BOOL TestCRC (U8 * MessagePU8 ,U16 MessageLengthU16);
LOCAL void TxComplete(void);
LOCAL void RxComplete(void);
LOCAL void TimerPeriodElapsed(void);


/******************************************************************************
 * GLOBAL FUNCTIONS : STANDARD FUNCTIONS
 *****************************************************************************/

/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR MODBUS_MASTER_STACK_InitR(void * rs485Thread)
{

    comportThread = rs485Thread;
    //ModbusMasterTimerU8 = TimerU8;
    AttemptLimitU8 = 3;

    //ModbusBaudRateU32 = BaudRateU32;

	//UART SETUP

	//baud rate must be set

	//Timer SETUP

	//according to baud rate
	//set prescaler, period  what about the frequecies

    //InterMessageIdleTimeU32 = (10/BaudRateU32)*3.5; // use to determine timer period


  	//Register Call-backs

    registerRs485TxCompleteCallBackWrapper(comportThread,TxComplete);
    registerRs485RxCompleteCallBackWrapper(comportThread,RxComplete);

    //API_UART_RegisterTxCpltCallback(ModbusMasterPortU8,TxComplete);
    //API_UART_RegisterRxCpltCallback(ModbusMasterPortU8,RxComplete);


    registerReceiveTimeOutCallBackWrapper(comportThread,TimerPeriodElapsed);
    registerTransmitTimeOutCallBackWrapper(comportThread,TimerPeriodElapsed);
    //API_TIMER_RegisterPeriodElapsedCallback(ModbusMasterTimerU8, TimerPeriodElapsed);
	//something
	return(SERROR_NONE);
}	//end MODBUS_MASTER_STACK_InitR()






/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR MODBUS_MASTER_STACK_TxRxR(MODBUS_MASTER_RX_TX_S * RequestS, CHAR * RxDataPC)
{

    MODBUS_MASTER_STACK_SilentTimeElapsedB = FALSE;
    U8 ModbusRxBuffer[256];
    U8 CRCU8[2];
    U8 ModbusTxBuffer[256];  //Max RTU message size = 256 Bytes

	memset(ModbusTxBuffer,0,256);
	memset(ModbusRxBuffer,0,256);
    memset(CRCU8,0,2);

    U8 RxCountU8 = 0;
	U16 CrcU16 = 0;

	U8 AttemptCountU8 = 0;

	//build message
	//Common to all modbus requests
	ModbusTxBuffer[0] = RequestS->SlaveAddressU8;
	ModbusTxBuffer[1] = RequestS->FunctionCodeU8;
	ModbusTxBuffer[2] = (U8)((RequestS->AddressU16 & 0xFF00)>>8);
	ModbusTxBuffer[3] = (U8)RequestS->AddressU16;
	ModbusTxBuffer[4] = (U8)((RequestS->RegisterCountU16 & 0xFF00)>>8);
	ModbusTxBuffer[5] = (U8)RequestS->RegisterCountU16;
	//Common to all modbus requests
    U8 transferSizeU8 = 0;







	if (RequestS->byteCountU8 > 0 )//in case of data being sent
	{
		ModbusTxBuffer[HEADERSIZE] = RequestS->byteCountU8;
		memcpy(ModbusTxBuffer + HEADERSIZE + sizeof(RequestS->byteCountU8),RequestS->txDataPU8,RequestS->byteCountU8);
		CrcU16 = usMBCRC16( ModbusTxBuffer, HEADERSIZE + sizeof(RequestS->byteCountU8) + RequestS->byteCountU8 );
		ModbusTxBuffer[HEADERSIZE + sizeof(RequestS->byteCountU8) + RequestS->byteCountU8] = (U8)(CrcU16 & 0xFF);//low byte
		ModbusTxBuffer[HEADERSIZE + sizeof(RequestS->byteCountU8) + RequestS->byteCountU8 + sizeof(U8)] = (U8)((CrcU16 & 0xFF00)>>8);//high byte

        transferSizeU8 = HEADERSIZE + sizeof(RequestS->byteCountU8) + RequestS->byteCountU8 + sizeof(U8)+1;
	}
	else
	{
        //calculate CRC
		CrcU16 = usMBCRC16( ModbusTxBuffer, HEADERSIZE);
		ModbusTxBuffer[HEADERSIZE] = (U8)(CrcU16 & 0xFF);//low byte
		ModbusTxBuffer[HEADERSIZE + sizeof(U8)] = (U8)((CrcU16 & 0xFF00)>>8);//high byte
        transferSizeU8 = HEADERSIZE + sizeof(U8) + 1;
	}

	//flush the buffer
    //API_UART_ClearRxBuffer(ModbusMasterPortU8);

	//Do this until the TxRx is a success or maximum number of attempts has been reached
    BOOL writePassedB = TRUE;
    BOOL readPassedB = TRUE;
    U8 expectedRxCountU8 = 0;
    while (AttemptCountU8 < AttemptLimitU8)
    {
//        if(MODBUS_MASTER_STACK_SilentTimeElapsedB == FALSE )
//        {
            //Check that  3.5 char passed since previous message
            //API_UART_TxDataR(ModbusMasterPortU8, ModbusTxBuffer, transferSizeU16);

            rs485WriteWrapper(comportThread, ModbusTxBuffer,transferSizeU8);

            while (MODBUS_MASTER_STACK_SilentTimeElapsedB == FALSE)//wait till sent
            {
                delayWrapper(comportThread, 10);
            }
            //reset the "end of message"/"silent time marker"
            MODBUS_MASTER_STACK_SilentTimeElapsedB = FALSE;

            //API_UART_RxDataCntR(ModbusMasterPortU8, &RxCountU16);
            //API_UART_RxDataR(ModbusMasterPortU8, ModbusRxBuffer, NULL, RxCountU16);
            RxCountU8 = rs485GetByteAvailableWrapper(comportThread);


            writePassedB = TRUE;
            readPassedB = TRUE;

            if (RequestS->byteCountU8>0) //if write
            {
                if (RxCountU8 != 8)
                {
                    writePassedB = FALSE;
                }
            }
            else
            {
                expectedRxCountU8 = 5 + RequestS->RegisterCountU16*2;
                if(RxCountU8 != expectedRxCountU8)
                {
                    readPassedB = FALSE;
                }
            }



            if (readPassedB && writePassedB)
            {

                rs485ReadWrapper(comportThread,ModbusRxBuffer,RxCountU8);


                //if crc doesn't match or no response has been received increment the attempt count
                if( TestCRC(ModbusRxBuffer,RxCountU8) == TRUE && (RxCountU8 != 0))
                {

                    memcpy(RxDataPC,ModbusRxBuffer,RxCountU8);//offsets are to strip headers and crc

                    return(SERROR_NONE);
                }
                else
                {
                    AttemptCountU8++;
                    //retry
                }


            }
            else
            {
              rs485ReadWrapper(comportThread,ModbusRxBuffer,RxCountU8);//just for debugging
              RxCountU8 = 0; //clear the buffer

              AttemptCountU8++;
            }


    }
	return(SERROR_TIMEOUT);
}	//end MODBUS_MASTER_STACK_TxRxR()


/******************************************************************************
 * GLOBAL FUNCTIONS : ADDITIONAL FUNCTIONS
 *****************************************************************************/

/******************************************************************************
 * See header file for details
 *****************************************************************************/
//U8 MODBUS_MASTER_STACK_AGlobalFuncU8(CHAR arg1C)
//{
//	return(1);
//}	//end MODBUS_MASTER_STACK_GlobalFuncU8()


/******************************************************************************
 * LOCAL FUNCTIONS
 *****************************************************************************/

LOCAL BOOL TestCRC (U8 * MessagePU8 ,U16 MessageLengthU16)
{
	U16 CrcU16 = usMBCRC16( MessagePU8, MessageLengthU16 -sizeof(CrcU16));;

	if (( (U8)(CrcU16 & 0xFF) == MessagePU8[MessageLengthU16-2]) && ( (U8)((CrcU16 & 0xFF00)>>8 )  == MessagePU8[MessageLengthU16-1]))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*****************************************************************************************
* Function Name		: TxComplete
* Description		:
*
* Arguments			: None
*
* Return			: None
*****************************************************************************************/
LOCAL void TxComplete(void)
{
    startModbusTransmitTimerWrapper(comportThread);
    //API_TIMER_EnableR(ModbusMasterTimerU8);

    MODBUS_MASTER_STACK_SilentTimeElapsedB = FALSE;
    //MODBUS_MASTER_STACK_AwaitingResponseB = TRUE;


	//Start for silent time
}


/*****************************************************************************************
* Function Name		: RxComplete
* Description		:
*
* Arguments			: None
*
* Return			: None
*****************************************************************************************/
LOCAL void RxComplete(void)
{
    startModbusReceiveTimerWrapper(comportThread);
    //API_TIMER_EnableR(ModbusMasterTimerU8);//Reload the  timer each time something comes in
}


//setup to trigger after 3.5 characters[]
LOCAL void TimerPeriodElapsed(void)
{

	MODBUS_MASTER_STACK_SilentTimeElapsedB = TRUE;

    //BOOL MODBUS_MASTER_STACK_AwaitingResponseB = FALSE;
}



#endif	//#ifndef _MODBUS_MASTER_STACK_C_
#ifdef __cplusplus
}
#endif
/******************************************************************************
 * END
 *****************************************************************************/
