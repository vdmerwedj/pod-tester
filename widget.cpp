#include "widget.h"
#include "ui_widget.h"
#include <QtSerialPort/QSerialPortInfo>
#include <QtSerialPort/QSerialPort>
#include <QIODevice>
#include <QTime>
#include <QThread>
#include <QFileDialog>
#include <stdio.h>
#include <QWindow>
#include <stdlib.h>
#include <QApplication>



extern "C"
{
#include "pod_detector.h"
#include "modbus_master_stack.h"
#include "modbus_manager.h"
#include "file_transfer_master.h"
}

#define CALIBRATION_POLL_AKNOWLEDGE_LIMIT               50

#define FILE_TYPE_TEST                                  0x00
#define FILE_TYPE_SOFTWARE_UPDATE                       0x01

#define SLAVE_ADDRESS           1

#define TIMER_PROPERTY_STATE                            "State"
#define TIMER_PROPERTY_STATE_WARMUP                     0x01
#define WARMUP_TIME                                     5
#define TIMER_PROPERTY_STATE_GAS_EXPOSURE               0x02



#define POD_TYPE_CO             4
#define POD_TYPE_O2             6


#define MODBUS_FUNCTION_READ_INPUT_STATUS               0x02
#define MODBUS_FUNCTION_READ_HOLDING_REGISTERS          0x03
#define MODBUS_FUNCTION_READ_READ_INPUT_REGISTERS       0x04
#define MODBUS_FUNCTION_WRITE_SINGLE_COIL               0x05
#define MODBUS_FUNCTION_WRITE_SINGLE_REGISTER           0x06
#define MODBUS_FUNCTION_WRITE_MULTIPLE_COILS            0x0F
#define MODBUS_FUNCTION_WRITE_MULTIPLE_REGISTERS        0x10


#define MODBUS_REGISTER_ADDRESS_LOGICAL_CAL_GAS         0x04
#define MODBUS_REGISTER_ADDRESS_CURRENT_C               0x1c
#define MODBUS_REGISTER_ADDRESS_CURRENT_M               0x20

#define MODBUS_REGISTER_ADDRESS_SOFTWARE_VERSION        0x3b

#define MODBUS_REGISTER_ADDRESS_CAL_EPOCH               0x37

#define MODBUS_REGISTER_ADDRESS_GET_GAS                 0x80
#define MODBUS_REGISTER_ADDRESS_UPTIME                  0x86
#define MODBUS_REGSITER_ADDRESS_CAL_REQ_BITS            0x8a
#define MODBUS_REGISTER_ADDRESS_CAL_ACK_BITS            0x8b


#define MODBUS_REGISTER_ADDRESS_RAW_ZERO                0x8c
#define MODBUS_REGISTER_ADDRESS_RAW_CAL_GAS             0x8e

#define MODBUS_REGISTER_ADDRESS_POD_TYPE                0xa0
#define MODBUS_REGISTER_ADDRESS_POD_ID                  0xa2



Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{



    ui->setupUi(this);

    ui->comboBoxRs485ComPort->installEventFilter(this);
    ui->comboBoxIrdaComPort->installEventFilter(this);


    ui->comboRs485->addItems({"9600","14400","19200","28800","38400","57600","115200"});

    ui->comboIrda->addItems({"9600","14400","19200","28800","38400","57600","115200"});
    ui->comboIrda->setCurrentIndex(5);

    ui->comboFileType->addItem("0: Test File",(QVariant)FILE_TYPE_TEST);
    ui->comboFileType->addItem("1: Software Update",(QVariant)FILE_TYPE_SOFTWARE_UPDATE);
    ui->comboFileType->setCurrentIndex(1);

    ui->comboBoxPodType->addItem("4: CO",(QVariant)POD_TYPE_CO);
    ui->comboBoxPodType->addItem("6: O2",(QVariant)POD_TYPE_O2);
    ui->comboBoxPodType->setCurrentIndex(0);

    ui->comboModbusFunctionCode->addItem("2: Read Discrete Inputs",(QVariant)MODBUS_FUNCTION_READ_INPUT_STATUS);
    ui->comboModbusFunctionCode->addItem("3: Read Holding Registers",(QVariant)MODBUS_FUNCTION_READ_HOLDING_REGISTERS);
    ui->comboModbusFunctionCode->addItem("4: Read Input Registers",(QVariant)MODBUS_FUNCTION_READ_READ_INPUT_REGISTERS);
    ui->comboModbusFunctionCode->addItem("5: Write Single Coil",(QVariant)MODBUS_FUNCTION_WRITE_SINGLE_COIL);
    ui->comboModbusFunctionCode->addItem("6: Write Single Register",(QVariant)MODBUS_FUNCTION_WRITE_SINGLE_REGISTER);
    ui->comboModbusFunctionCode->addItem("15: Write Multiple Coils",(QVariant)MODBUS_FUNCTION_WRITE_MULTIPLE_COILS);
    ui->comboModbusFunctionCode->addItem("16: Write Multiple Registers",(QVariant)MODBUS_FUNCTION_WRITE_MULTIPLE_REGISTERS);




    ui->comboBoxLogFilePath->addItem(QDir::currentPath(),NULL);
    ui->comboBoxLogFilePath->setCurrentIndex(0);






    ui->comboModbusFunctionCode->setCurrentIndex(1);



    ui->comboRegisterAddress->addItem("0x04: Logical Cal Gas",(QVariant)MODBUS_REGISTER_ADDRESS_LOGICAL_CAL_GAS);
    ui->comboRegisterAddress->addItem("0x1c: Current C",(QVariant)MODBUS_REGISTER_ADDRESS_CURRENT_C);
    ui->comboRegisterAddress->addItem("0x20: Current M",(QVariant)MODBUS_REGISTER_ADDRESS_CURRENT_M);
    ui->comboRegisterAddress->addItem("0x3b: Software Version",(QVariant)MODBUS_REGISTER_ADDRESS_SOFTWARE_VERSION);
    ui->comboRegisterAddress->addItem("0x37: Last Calibration Time",(QVariant)MODBUS_REGISTER_ADDRESS_CAL_EPOCH);
    ui->comboRegisterAddress->addItem("0x80: Get Gas",(QVariant)MODBUS_REGISTER_ADDRESS_GET_GAS);
    ui->comboRegisterAddress->addItem("0x86: Uptime",(QVariant)MODBUS_REGISTER_ADDRESS_UPTIME);
    ui->comboRegisterAddress->addItem("0x8a: Calibration Request Bits",(QVariant)MODBUS_REGSITER_ADDRESS_CAL_REQ_BITS);
    ui->comboRegisterAddress->addItem("0x8b: Calibration Aknowledge Bits",(QVariant)MODBUS_REGISTER_ADDRESS_CAL_ACK_BITS);
    ui->comboRegisterAddress->addItem("0x8c: Raw Zero",(QVariant)MODBUS_REGISTER_ADDRESS_RAW_ZERO);
    ui->comboRegisterAddress->addItem("0x8e: Raw Cal Gas",(QVariant)MODBUS_REGISTER_ADDRESS_RAW_CAL_GAS);
    ui->comboRegisterAddress->addItem("0xa0: Pod Type",(QVariant)MODBUS_REGISTER_ADDRESS_POD_TYPE);
    ui->comboRegisterAddress->addItem("0xa2: Pod Id",(QVariant)MODBUS_REGISTER_ADDRESS_POD_ID);

    ui->comboRegisterAddress->setCurrentIndex(6);

    ui->comboRegisterCount->addItems({"1","2","4"});
    ui->comboRegisterCount->setCurrentIndex(1);

    ui->spinBoxModbusTimeOut->setValue(20);
    connect(ui->btnConnect,SIGNAL(released()),this,SLOT(handleBtnConnect()));
    connect(ui->btnMeasure,SIGNAL(released()),this,SLOT(handleBtnMeasure()));

    connect(ui->btnCalibrate,SIGNAL(released()),this,SLOT(handleBtnCalibrate()));
    connect(ui->btnTakeZero,SIGNAL(released()),this,SLOT(handleBtnTakeZero()));
    connect(ui->btnExposeSensor,SIGNAL(released()),this,SLOT(handleBtnExposeSensor()));

    connect(ui->btnTakeGas,SIGNAL(released()),this,SLOT(handleBtnTakeGas()));
    connect(ui->btnFinish,SIGNAL(released()),this,SLOT(handleBtnFinish()));

    connect(ui->btnSetPodType,SIGNAL(released()),this,SLOT(handleBtnSetPodType()));
    connect(ui->btnGetPodType,SIGNAL(released()),this,SLOT(handleBtnGetPodType()));

    connect(ui->btnSetSerialNumber,SIGNAL(released()),this,SLOT(handleBtnSetSerialNumber()));
    connect(ui->btnGetSerialNumber,SIGNAL(released()),this,SLOT(handleBtnGetSerialNumber()));

    connect(ui->btnStartStreaming,SIGNAL(released()),this,SLOT(handleBtnStartStreaming()));


    connect(ui->btnSelectLogFile,SIGNAL(released()),this,SLOT(handleBtnSelectLogFile()));



    ui->lineEditSerial->setValidator(new QIntValidator(0,65535,this));


    connect(ui->btnSelectFile,SIGNAL(released()),this, SLOT(handleBtnSelectFile()));
    connect(ui->btnSendFile,SIGNAL(released()),this,SLOT(handleBtnSendFile()));

    connect(ui->comboFile, SIGNAL(currentIndexChanged(int)),this, SLOT(handleComboFileChanged()));


    connect(ui->lineEditModbusData,SIGNAL(textChanged(QString)),this,SLOT(handleLineEditModbusData()));



    connect(&irdaPort,&QSerialPort::readyRead,this,&Widget::irdaRxComplete);
    connect(&rs485Port,&QSerialPort::readyRead,this,&Widget::rs485RxComplete);
    //connect(&irdaPort,&QSerialPort::bytesWritten,this,&Widget::irdaTxComplete);
    connect(&rs485Port,&QSerialPort::bytesWritten,this,&Widget::rs485TxComplete);



    connect(&streamingTimer, &QTimer::timeout,this,&Widget::streamingTimerSlot);
    connect(&modbusReceiveTimer, &QTimer::timeout,this,&Widget::modbusReceiveTimeOutSlot);
    connect(&modbusTransmitTimer, &QTimer::timeout,this,&Widget::modbusTransmitTimeOutSlot);
    connect(&heartbeatTimer, &QTimer::timeout,this,&Widget::heartbeatTimeOutSlot);
    connect(&countdownTimer,&QTimer::timeout,this,&Widget::countdownTimeOutSlot);



    countdownTimer.setInterval(1000);
    ui->lcdNumberCountdown->display(WARMUP_TIME);



            seriesLogging = new QSplineSeries();
            seriesLogging->setName("spline");

            chartLogging = new QChart();
            chartLogging->legend()->hide();
            chartLogging->addSeries(seriesLogging);
            chartLogging->setTitle("Gas Sample Values");
            chartLogging->createDefaultAxes();
            //chartLogging->axisY()->setRange(0, 2000);

            chartLogging->axisY()->setVisible(false);

            chartLogging->axisX()->setRange(-120,0);
            chartLogging->axisX()->setTitleText("Time (s):");






            ui->chartView->setChart(chartLogging);
            ui->chartView->setRenderHint(QPainter::Antialiasing);


            const auto infos = QSerialPortInfo::availablePorts();


            for (const QSerialPortInfo &info : infos)
            {
                ui->comboBoxIrdaComPort->addItem(info.portName() +" " + info.description());
                ui->comboBoxRs485ComPort->addItem(info.portName() +" " + info.description());
            }

this->setWindowTitle(this->windowTitle()+"  V"+APP_VERSION);


}



Widget::~Widget()
{
    delete ui;
}


bool Widget::eventFilter(QObject* o, QEvent* e)
{
    if(e->type() == QEvent::HoverEnter)
    {

            refreshComboBoxRs485ComPort();
            refreshComboBoxIrdaComPort();



        return true;
    }
    else
    {
        return false;
    }
}


void Widget::handleBtnConnect()
{

    if (ui->btnConnect->text()=="Connect")
    {
        countdownTimer.setProperty((char const *)&TIMER_PROPERTY_STATE,TIMER_PROPERTY_STATE_WARMUP);


        double interval = (double)(ui->spinBoxModbusTimeOut->value())*10/ui->comboRs485->currentText().toDouble()*1000;

        streamingTimer.setInterval(1000);
        streamingTimer.setSingleShot(false);
        modbusTransmitTimer.setInterval(1000);
        modbusTransmitTimer.setSingleShot(true);
        modbusReceiveTimer.setInterval((int)interval);
        modbusReceiveTimer.setSingleShot(true);

        heartbeatTimer.setInterval(5000);
        heartbeatTimer.setSingleShot(true);



        POD_DETECTOR_InitR(this);
        MODBUS_MASTER_STACK_InitR(this);


        const auto infos = QSerialPortInfo::availablePorts();


        for (const QSerialPortInfo &info : infos)
        {

            if ((info.portName() +" " + info.description()) == ui->comboBoxIrdaComPort->currentText()) //"A600AMITA")
            {
                        irdaPort.close();
                        irdaPort.setPortName(info.portName());


                        if(!irdaPort.open(QIODevice::ReadWrite))
                        {
                            widgetError.showMessage("Could not connect to Irda port. Please try again.");
                            rs485Port.close();
                            return;
                        }
                        irdaPort.setBaudRate(ui->comboIrda->currentText().toInt(),irdaPort.AllDirections);

             }

            if ((info.portName() +" " + info.description()) ==  ui->comboBoxRs485ComPort->currentText())       // "FTJRNSKBA")
            {
                        rs485Port.close();
                        rs485Port.setPortName(info.portName());


                        if (!rs485Port.open(QIODevice::ReadWrite))
                        {
                            widgetError.showMessage("Could not connect to RS 485 port. Please try again.");
                            irdaPort.close();
                            return;
                        }
                        rs485Port.setBaudRate(ui->comboRs485->currentText().toInt(),rs485Port.AllDirections);


            }
        }



        ui->comboRs485->setEnabled(false);
        ui->spinBoxModbusTimeOut->setEnabled(false);
        ui->btnConnect->setText("Disconnect");

    }
    else
    {
        ui->spinBoxModbusTimeOut->setEnabled(true);
        ui->comboRs485->setEnabled(true);
        ui->btnConnect->setText("Connect");
        ui->btnMeasure->setEnabled(false);

        ui->btnSetPodType->setEnabled(false);
        ui->btnSetSerialNumber->setEnabled(false);

        ui->btnGetPodType->setEnabled(false);
        ui->btnGetSerialNumber->setEnabled(false);


        ui->btnCalibrate->setEnabled(false);
        ui->btnTakeZero->setEnabled(false);
        ui->btnExposeSensor->setEnabled(false);
        ui->btnTakeGas->setEnabled(false);
        ui->btnFinish->setEnabled(false);


        ui->labelCountdown->setText("Waiting for connection...");
        QPalette paletteLCD;

        paletteLCD.setColor(QPalette::WindowText,Qt::red);

        ui->lcdNumberCountdown->setPalette(paletteLCD);

        ui->lcdNumberCountdown->display(WARMUP_TIME);

        ui->btnSendFile->setEnabled(false);
        ui->comboModbusFunctionCode->setEnabled(false);
        ui->comboRegisterAddress->setEnabled(false);
        ui->comboRegisterCount->setEnabled(false);
        ui->lineEditModbusData->setEnabled(false);
        ui->spinBoxModbusByteCount->setEnabled(false);


        if (streamingTimer.isActive())
        {
            handleBtnStartStreaming();
        }


        irdaPort.close();
        rs485Port.close();

        heartbeatTimer.stop();

    }
}

void Widget::handleBtnMeasure()
{
    MODBUS_MASTER_RX_TX_S measurement;

        measurement.AddressU16 =        ui->comboRegisterAddress->itemData(ui->comboRegisterAddress->currentIndex(),Qt::UserRole).toInt();
        measurement.byteCountU8 =       ui->spinBoxModbusByteCount->value();
        measurement.FunctionCodeU8 =    ui->comboModbusFunctionCode->itemData(ui->comboModbusFunctionCode->currentIndex(),Qt::UserRole).toInt();
        measurement.RegisterCountU16 =  ui->comboRegisterCount->currentText().toInt();
        measurement.SlaveAddressU8 = SLAVE_ADDRESS;

        measurement.txDataPU8 = (U8*)&(ui->lineEditModbusData->text());

        CHAR  response[100];
        memset(response,0,sizeof(response));


    MODBUS_MASTER_STACK_TxRxR(&measurement,response);
}
///

void Widget::handleBtnSetSerialNumber()
{
    MODBUS_MASTER_RX_TX_S measurement;
    U8 data[2];
    memset(data,0,sizeof(data));


    CHAR  rxdata[100];
    memset(rxdata,0,sizeof(rxdata));

    //set pod type

    measurement.FunctionCodeU8 = MODBUS_FUNCTION_WRITE_MULTIPLE_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_POD_ID;
    measurement.RegisterCountU16 = sizeof(U16)/2;
    measurement.byteCountU8 = 2*measurement.RegisterCountU16;

    U16 serial = ui->lineEditSerial->text().toInt();
    memcpy(data,&serial,sizeof(serial));

    data[0] = serial>>8;
    data[1] = serial& 0xff;

    measurement.txDataPU8 = (U8*)data;
    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//set pod type
}


void Widget::handleBtnStartStreaming()
{
    if (streamingTimer.isActive())
    {

        streamingTimer.stop();
        ui->btnStartStreaming->setText("Start Streaming");
    }
    else
    {
        loggingStartTime = QDateTime::currentSecsSinceEpoch();

        logFilePath = ui->comboBoxLogFilePath->currentText().toStdString()+"/" +QString::number(loggingStartTime).toStdString()+".csv";

        FILE * streamingLogFile = fopen(logFilePath.c_str(),"w");

        if (streamingLogFile == NULL)
        {
            widgetError.showMessage("Creation of log file failed. Please make sure that the file is not open in another program and try again.");

            return;
        }
        fclose(streamingLogFile);


       seriesLogging->clear();
       ui->btnStartStreaming->setText("Stop Streaming");
       streamingTimer.start();
       chartLogging->axisY()->setVisible(true);
    }
}



void Widget::handleBtnSelectLogFile()
{
    ui->comboBoxLogFilePath->addItem(QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                              QDir::currentPath(),
                                                              QFileDialog::ShowDirsOnly
                                                              | QFileDialog::DontResolveSymlinks),NULL);
    ui->comboFile->setCurrentIndex(ui->comboFile->count()-1);



}


void Widget::handleBtnGetSerialNumber()
{
    MODBUS_MASTER_RX_TX_S measurement;
    U8 data[2];
    memset(data,0,sizeof(data));


    CHAR  rxdata[100];
    memset(rxdata,0,sizeof(rxdata));

    //set pod type

    measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_POD_ID;
    measurement.RegisterCountU16 = sizeof(U16);

    measurement.byteCountU8 = 0;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//set pod type

    U8 serialAU8[4] = {0,0,0,0};

    byteSwap(serialAU8,rxdata+3,rxdata[2]);


    U32 serialU32 = 0;
    memcpy(&serialU32,serialAU8,sizeof(U32));

    ui->lineEditSerial->setText(QString::number(serialU32));
}
///
void Widget::handleBtnSetPodType()
{
    MODBUS_MASTER_RX_TX_S measurement;
    U8 data[8];
    memset(data,0,sizeof(data));


    CHAR  rxdata[100];
    memset(rxdata,0,sizeof(rxdata));

    //set pod type

    measurement.FunctionCodeU8 = MODBUS_FUNCTION_WRITE_MULTIPLE_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_POD_TYPE;
    measurement.RegisterCountU16 = sizeof(U16)/2;
    measurement.byteCountU8 = 2*measurement.RegisterCountU16;

    U16 podType = ui->comboBoxPodType->itemData(ui->comboBoxPodType->currentIndex(),Qt::UserRole).toInt();
    //memcpy(data,&podType,sizeof(podType));
    data[0] = 0;
    data[1] = podType;
    measurement.txDataPU8 = (U8*)data;
    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//set pod type

    handleBtnGetPodType();

}

void Widget::handleBtnGetPodType()
{
    MODBUS_MASTER_RX_TX_S measurement;
    U8 data[8];
    memset(data,0,sizeof(data));


    CHAR  rxdata[100];
    memset(rxdata,0,sizeof(rxdata));

    //set pod type

    measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_POD_TYPE;
    measurement.RegisterCountU16 = sizeof(U16)/2;
    measurement.byteCountU8 = 0;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//set pod type

    switch (rxdata[4]) {
    case POD_TYPE_CO:

        ui->comboBoxPodType->setCurrentIndex(0);
        ui->doubleSpinBoxLogicalCalGas->setMaximum(1000000);
        ui->labelLogicalCalGas->setText("Logical Cal Gas (ppm):");
        seriesLogging->setName("CO ppm");
        chartLogging->legend()->setVisible(true);
        ui->spinBoxGasExposureTime->setValue(60);
        break;
    case POD_TYPE_O2:

        ui->comboBoxPodType->setCurrentIndex(1);
        ui->doubleSpinBoxLogicalCalGas->setMaximum(100);
        ui->labelLogicalCalGas->setText("Logical Cal Gas (%):");
        seriesLogging->setName("O2 %");
        chartLogging->legend()->setVisible(true);
        ui->spinBoxGasExposureTime->setValue(70);
        break;
    default:
        ui->comboBoxPodType->setCurrentIndex(0);
        ui->labelLogicalCalGas->setText("Logical Cal Gas (ppm):");
        seriesLogging->setName("CO ppm");
        chartLogging->legend()->setVisible(true);
        ui->spinBoxGasExposureTime->setValue(60);
        break;
    }
}



void Widget::handleBtnCalibrate()
{
    MODBUS_MASTER_RX_TX_S measurement;
    U8 data[8];
    memset(data,0,sizeof(data));


    CHAR  rxdata[100];
    memset(rxdata,0,sizeof(rxdata));

    //set logical cal gas

    measurement.FunctionCodeU8 = MODBUS_FUNCTION_WRITE_MULTIPLE_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_LOGICAL_CAL_GAS;
    measurement.RegisterCountU16 = sizeof(double)/2;
    measurement.byteCountU8 = 2*measurement.RegisterCountU16;

    double logicalCalGas = ui->doubleSpinBoxLogicalCalGas->value();

    U8 logicalCalGasBytes[8];
    memset(logicalCalGasBytes,0,sizeof(double));

    byteSwap(logicalCalGasBytes,&logicalCalGas,sizeof(double));
    memcpy(data,logicalCalGasBytes,sizeof(logicalCalGasBytes));


    measurement.txDataPU8 = (U8*)data;
    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//set logical cal gas

        measurement.FunctionCodeU8 = MODBUS_FUNCTION_WRITE_MULTIPLE_REGISTERS;
        measurement.SlaveAddressU8 = SLAVE_ADDRESS;
        measurement.AddressU16 = MODBUS_REGSITER_ADDRESS_CAL_REQ_BITS;
        measurement.RegisterCountU16 = 1;
        measurement.byteCountU8 = 2*measurement.RegisterCountU16;
        data[0] = 0;
        data[1] = 1;

        measurement.txDataPU8 = (U8*)&data;
        MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//request


        ui->btnCalibrate->setEnabled(false);

        measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
        measurement.SlaveAddressU8 = SLAVE_ADDRESS;
        measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_CAL_ACK_BITS;
        measurement.RegisterCountU16 = 1;
        measurement.byteCountU8 = 0;

        int attemptCount = 0;

        while (!(rxdata[4] & 0x01) && (attemptCount <    CALIBRATION_POLL_AKNOWLEDGE_LIMIT)) {

        MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//poll for acknowledge

        if (rxdata[4] & 0x01)
        {

            ui->btnTakeZero->setEnabled(true);
            ui->labelCountdown->setText("Take a zero sample.");
    }
        delay(100);//allow other tasks to complete
        attemptCount++;
        }

        if (attemptCount >=    CALIBRATION_POLL_AKNOWLEDGE_LIMIT)
        {
            ui->btnCalibrate->setEnabled(true);
        }

}

void Widget::handleBtnTakeZero()
{
    MODBUS_MASTER_RX_TX_S measurement;
    U8 data[2]= {0,0};


    CHAR  rxdata[100];
    memset(rxdata,0,sizeof(rxdata));




    measurement.FunctionCodeU8 = MODBUS_FUNCTION_WRITE_MULTIPLE_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGSITER_ADDRESS_CAL_REQ_BITS;
    measurement.RegisterCountU16 = 1;
    measurement.byteCountU8 = 2*measurement.RegisterCountU16;

    data[0] = 0;
    data[1] = 2;

    measurement.txDataPU8 = (U8*)&data;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);


    ui->btnTakeZero->setEnabled(false);

    measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_CAL_ACK_BITS;
    measurement.RegisterCountU16 = 1;
    measurement.byteCountU8 = 0;

    int attemptCount = 0;
    while (!(rxdata[4] & 0x02) && (attemptCount < CALIBRATION_POLL_AKNOWLEDGE_LIMIT)) {

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);


    if (rxdata[4] & 0x02)
    {

        ui->btnExposeSensor->setEnabled(true);
        ui->labelCountdown->setText("Connect the sensor to \n the refence gas.");

    }
    delay(100);//allow other tasks to complete
    attemptCount++;
    }

    if (attemptCount >=    CALIBRATION_POLL_AKNOWLEDGE_LIMIT)
    {
        ui->btnTakeZero->setEnabled(true);
    }


}



void Widget::handleBtnExposeSensor()
{
    ui->btnExposeSensor->setEnabled(false);
    countdownTimer.blockSignals(false);

    QPalette paletteLCD;

    paletteLCD.setColor(QPalette::WindowText,Qt::red);

    ui->lcdNumberCountdown->setPalette(paletteLCD);

    ui->lcdNumberCountdown->display(ui->spinBoxGasExposureTime->value());

    countdownTimer.setProperty((char const *)&TIMER_PROPERTY_STATE,TIMER_PROPERTY_STATE_GAS_EXPOSURE);

    countdownTimer.start();
    ui->labelCountdown->setText("Keep sensor exposed to gas...");

}

void Widget::handleBtnTakeGas()
{
    MODBUS_MASTER_RX_TX_S measurement;
    U8 data[2]= {0,0};

    CHAR  rxdata[100];
    memset(rxdata,0,sizeof(rxdata));


    measurement.FunctionCodeU8 = MODBUS_FUNCTION_WRITE_MULTIPLE_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGSITER_ADDRESS_CAL_REQ_BITS;
    measurement.RegisterCountU16 = 1;
    measurement.byteCountU8 = 2*measurement.RegisterCountU16;

    data[0] = 0;
    data[1] = 4;

    measurement.txDataPU8 = (U8*)&data;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);

    ui->btnTakeGas->setEnabled(false);

    measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_CAL_ACK_BITS;
    measurement.RegisterCountU16 = 1;
    measurement.byteCountU8 = 0;

    int attemptCount = 0;

    while (!(rxdata[4] & 0x04) && (attemptCount< CALIBRATION_POLL_AKNOWLEDGE_LIMIT)) {

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);


    if (rxdata[4] & 0x04)
    {

        ui->btnFinish->setEnabled(true);
        ui->labelCountdown->setText("Sample taken. \n Request the results.");

    }
    delay(100);//allow other tasks to complete
    attemptCount++;
    }
    if (attemptCount >=    CALIBRATION_POLL_AKNOWLEDGE_LIMIT)
    {
        ui->btnTakeGas->setEnabled(true);
    }
}

void Widget::handleBtnFinish()
{
    MODBUS_MASTER_RX_TX_S measurement;
    U8 data[2]= {0,0};


    CHAR  rxdata[100];
    memset(rxdata,0,sizeof(rxdata));



    measurement.FunctionCodeU8 = MODBUS_FUNCTION_WRITE_MULTIPLE_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGSITER_ADDRESS_CAL_REQ_BITS;
    measurement.RegisterCountU16 = 1;
    measurement.byteCountU8 = 2*measurement.RegisterCountU16;

    data[0] = 0;
    data[1] = 8;

    measurement.txDataPU8 = (U8*)&data;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//request calc MC


    ui->btnFinish->setEnabled(false);

    measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_CAL_ACK_BITS;
    measurement.RegisterCountU16 = 1;
    measurement.byteCountU8 = 0;

    int attemptCount = 0;

    while (!(rxdata[4] & 0x08) && (attemptCount <CALIBRATION_POLL_AKNOWLEDGE_LIMIT)) {



    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//check status


//    if (rxdata[4] & 0x08)
//    {



//    }
    delay(100);//allow other tasks to complete
    attemptCount++;
    }

    if (attemptCount >=    CALIBRATION_POLL_AKNOWLEDGE_LIMIT)
    {
        ui->btnFinish->setEnabled(true);
    }

    bool error = false;
    if (rxdata[3] & 0x80)
    {
        error = true;
    }

    //Request finish:




    measurement.FunctionCodeU8 = MODBUS_FUNCTION_WRITE_MULTIPLE_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGSITER_ADDRESS_CAL_REQ_BITS;
    measurement.RegisterCountU16 = 1;
    measurement.byteCountU8 = 2*measurement.RegisterCountU16;

    data[0] = 0;
    data[1] = 0x10;

    measurement.txDataPU8 = (U8*)&data;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//request finish


    measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_CAL_ACK_BITS;
    measurement.RegisterCountU16 = 1;
    measurement.byteCountU8 = 0;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//check status
    attemptCount = 0;




    while (rxdata[4] != 0 && (attemptCount< CALIBRATION_POLL_AKNOWLEDGE_LIMIT))
    {


        MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//check status


        if ((rxdata[4] == 0) && (attemptCount < CALIBRATION_POLL_AKNOWLEDGE_LIMIT))
        {

//            ui->btnCalibrate->setEnabled(true);
//            ui->labelCountdown->setText("Sensor ready! \n Enter calibration mode.");

        }

    delay(100);//allow other tasks to complete
    attemptCount++;
    }

    if (attemptCount >=    CALIBRATION_POLL_AKNOWLEDGE_LIMIT)
    {
        ui->btnFinish->setEnabled(true);
    }




//    QDateTime calibrationTime;
//    QTime now;

//    calibrationTime.setTime(now.currentTime());

    U64 timeU64 =  QDateTime::currentSecsSinceEpoch();

    measurement.FunctionCodeU8 = MODBUS_FUNCTION_WRITE_MULTIPLE_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16     = MODBUS_REGISTER_ADDRESS_CAL_EPOCH;
    measurement.RegisterCountU16 = sizeof(U32)/2;
    measurement.byteCountU8 = sizeof(U32);

    U8 timeAU8[4];

    byteSwap(timeAU8,&timeU64, sizeof(timeAU8));

    measurement.txDataPU8 = timeAU8;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);


    ///

    measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_RAW_CAL_GAS;
    measurement.RegisterCountU16 = 2;
    measurement.byteCountU8 = 0;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//get raw cal gas

    U32 rawCalGas = 0;


    U8 rawCalGasBytes[4] = {0,0,0,0};


    byteSwap(rawCalGasBytes,rxdata+3,rxdata[2]);


    memcpy(&rawCalGas,rawCalGasBytes,rxdata[2]);


    ui->labelRawCalGas->setText(QString::number(rawCalGas));



    measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_CURRENT_C;
    measurement.RegisterCountU16 = 4;
    measurement.byteCountU8 = 0;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);// get current C

    double currentC = 0;

    U8 currentCBytes[8];
    memset(currentCBytes,0,sizeof(double));

    byteSwap(currentCBytes,rxdata+3,rxdata[2]);

    memcpy(&currentC,currentCBytes,rxdata[2]);


    ui->labelCurrentC->setText(QString::number(currentC));

    measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_CURRENT_M;
    measurement.RegisterCountU16 = 4;
    measurement.byteCountU8 = 0;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//get current M

    double currentM = 0;

    U8 currentMBytes[8];

    memset(currentMBytes,0,sizeof(double));


    byteSwap(currentMBytes,rxdata+3,rxdata[2]);
    memcpy(&currentM,currentMBytes,rxdata[2]);

/// get last calibration (update)
///
    measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_CAL_EPOCH;
    measurement.RegisterCountU16 = sizeof(U32)/2;
    measurement.byteCountU8 = 0;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//get current M

    U32 epoch = 0;
    U8 epochA[4] = {0,0,0,0};

    byteSwap(epochA,rxdata+3,rxdata[2]);

    memcpy(&epoch,epochA,sizeof(U32));

    QDateTime displayTime;

    displayTime.setTime_t(epoch);



    ui->labelLastCalibrated->setText(displayTime.toString("yyyy-MM-dd hh:mm:ss"));
///


    ui->labelCurrentM->setText(QString::number(currentM));

    if (error == true)
    {
        ui->labelResult->setText("Error Occured");
    }
    else
    {
        ui->labelResult->setText("Success");
    }

    ui->btnCalibrate->setEnabled(true);
    ui->labelCountdown->setText("Sensor ready! \n Enter calibration mode.");


}



void Widget::handleBtnSelectFile()
{
    ui->comboFile->addItem(QFileDialog::getOpenFileName(
              this,
              "Open Document",
              QDir::currentPath(),
              "All files (*.*) ;; Document files (*.doc *.rtf);; PNG files (*.png)"),NULL);
    ui->comboFile->setCurrentIndex(ui->comboFile->count()-1);

}

void Widget::handleBtnSendFile()
{

    ui->btnSendFile->setEnabled(false);
    QByteArray filePath;

    filePath = ui->comboFile->currentText().toLatin1();



    if (ui->comboFile->currentText().isEmpty())
    {
        widgetError.showMessage("Please select a file to send to the POD and try again.");
        return;
    }

    if (ui->comboFileType->itemData(ui->comboFileType->currentIndex(),Qt::UserRole).toInt() == FILE_TYPE_SOFTWARE_UPDATE
            && !filePath.contains(".pod"))
    {


        widgetError.showMessage("Only .pod files are allowed for updates. Please select another file or file type.");
        return;
    }



    FILE_TRANSFER_start((CHAR*)filePath.data(),1,ui->comboFileType->itemData(ui->comboFileType->currentIndex(),Qt::UserRole).toInt(),this);

    ui->btnSendFile->setEnabled(true);
}


void Widget::refreshComboBoxRs485ComPort()
{
    const auto infos = QSerialPortInfo::availablePorts();

    QString currentPort;

    currentPort = ui->comboBoxRs485ComPort->currentText();

    ui->comboBoxRs485ComPort->clear();
    int i = 0;
    int currentItem = 0;

    for (const QSerialPortInfo &info : infos)
    {
        if (currentPort == (info.portName() +" " + info.description()))
        {
            currentItem = i;
        }
        ui->comboBoxRs485ComPort->addItem(info.portName() +" " + info.description());

        i++;
    }

    ui->comboBoxRs485ComPort->setCurrentIndex(currentItem);


}

void Widget::refreshComboBoxIrdaComPort()
{
    const auto infos = QSerialPortInfo::availablePorts();

    QString currentPort;

    currentPort = ui->comboBoxIrdaComPort->currentText() ;

    ui->comboBoxIrdaComPort->clear();
    int i = 0;
    int currentItem = 0;

    for (const QSerialPortInfo &info : infos)
    {
        if (currentPort == (info.portName()  +" " + info.description()))
        {
            currentItem = i;
        }
        ui->comboBoxIrdaComPort->addItem(info.portName() +" " + info.description());

        i++;
    }

    ui->comboBoxIrdaComPort->setCurrentIndex(currentItem);


}


void Widget::handleComboFileChanged()
{
    if (ui->comboFile->currentText().isEmpty())
    {
        ui->btnSendFile->setEnabled(false);
    }
    else if (ui->btnMeasure->isEnabled())
    {
        ui->btnSendFile->setEnabled(true);
    }
}




void Widget::handleLineEditModbusData()
{
    ui->spinBoxModbusByteCount->setValue(ui->lineEditModbusData->text().length());
}

////Read/Write
void Widget::irdaRead(CHAR * rxPC,U8 lengthU8)
{
    //qInfo("Entering IRDA read.");
    irdaPort.read((char*)rxPC,lengthU8);

    QByteArray rxData;
    QString rxDataString;
    QString temp;


    rxData.setRawData((char*)rxPC,lengthU8);


    temp = rxData.toHex();

    for (int i =0;i<temp.count();i++)
    {
        rxDataString.append(temp.at(i));

        if ((i+1)%2==0 && i!=0)
        {
            rxDataString.append(" ");
        }

    }

    QTime myTime;
    ui->txtBrowseIrdaRxTx->append(myTime.currentTime().toString()+" RX: "+rxDataString.toUpper());
    return;
}

void irdaReadWrapper(void * master, CHAR * rxPC, U8 lengthU8)
{
    reinterpret_cast<Widget*>(master)->irdaRead(rxPC,lengthU8);
}

void Widget::irdaWrite(CHAR * txPC, U8 lengthU8)
{
    //qInfo("Entering IRDA write.");
    irdaPort.write((char *)txPC,lengthU8);


    QByteArray txData;
    QString txDataString;
    QString temp;


    txData.setRawData((char*)txPC,lengthU8);


    temp = txData.toHex();

    for (int i =0;i<temp.count();i++)
    {
        txDataString.append(temp.at(i));

        if ((i+1)%2==0 && i!=0)
        {
            txDataString.append(" ");
        }

    }

    QTime myTime;

    ui->txtBrowseIrdaRxTx->append(myTime.currentTime().toString()+" TX: "+txDataString.toUpper());
    return;
}

void irdaWriteWrapper(void * master, CHAR * txPC, U8 lengthU8)
{
    reinterpret_cast<Widget*>(master)->irdaWrite(txPC,lengthU8);
}

void Widget::rs485Read(CHAR * rxPC,U8 lengthU8)
{

    //qInfo("read enter");






//    if (&rs485Port != NULL)
//    {
        rs485Port.read((char*)rxPC,lengthU8);
//    }


        //qInfo("port read");

    QByteArray rxData;
    QString rxDataString;
    QString temp;

    rxData.setRawData((char*)rxPC,lengthU8);



    temp = rxData.toHex();
    //qInfo("Pad the output.");
    for (int i =0;i<temp.count();i++)
    {
        rxDataString.append(temp.at(i));

        if ((i+1)%2==0 && i!=0)
        {
            rxDataString.append(" ");
        }

    }

    QTime myTime;
    //qInfo("info output");
    ui->txtBrowseRs485RxTx->append(myTime.currentTime().toString()+" RX: "+rxDataString.toUpper());
    return;
}

void rs485ReadWrapper(void * master, CHAR * rxPC, U8 lengthU8)
{
    //qInfo("Wrapper enter");
    if (master!= NULL)
    {
        reinterpret_cast<Widget*>(master)->rs485Read(rxPC,lengthU8);
    }
}

void Widget::rs485Write(CHAR * txPC, U8 lengthU8)
{
    //qInfo("Entering RS 485 Write.");
    rs485Port.write((char *)txPC,lengthU8);



    QByteArray txData;
    QString txDataString;
    QString temp;


    txData.setRawData((char*)txPC,lengthU8);


    temp = txData.toHex();

    for (int i =0;i<temp.count();i++)
    {
        txDataString.append(temp.at(i));

        if ((i+1)%2==0 && i!=0)
        {
            txDataString.append(" ");
        }

    }

    QTime myTime;
    ui->txtBrowseRs485RxTx->append(myTime.currentTime().toString()+" TX: "+txDataString.toUpper());
    return;
}


void rs485WriteWrapper(void * master, CHAR * txPC, U8 lengthU8)
{
    reinterpret_cast<Widget*>(master)->rs485Write(txPC,lengthU8);
}

int Widget::rs485GetByteAvailable()
{
    int bytes = rs485Port.bytesAvailable();
    //qInfo("Bytecount received.");
    return bytes;
}

int rs485GetByteAvailableWrapper(void * master)
{
    //qInfo("Call rs485GetByteAvailableWrapper");
    return reinterpret_cast<Widget*>(master)->rs485GetByteAvailable();
}


////Callbacks

void Widget::irdaRxComplete()
{
    //qInfo("IRDA TX complete.");
    if (irdaRxCompleteCallBack != NULL)
    {
        irdaRxCompleteCallBack();
    }

}

void Widget::rs485RxComplete()
{
    //qInfo("RS 485 RX complete.");
   // qInfo("rx in");
    int bytes = rs485Port.bytesAvailable();
    //qInfo("rx byte cout received");
    if (bytes>0)
    {
        if (rs485RxCompleteCallBack != NULL)
        {
            //qInfo("entering rx complete callback.");
            rs485RxCompleteCallBack();
        }
    }


}

void Widget::irdaTxComplete()
{
//    if (irdaTxCompleteCallBack != NULL)
//    {
//        irdaTxCompleteCallBack();
//    }
}

void Widget::rs485TxComplete()
{
    //qInfo("RS 485 TX complete.");
    if (rs485TxCompleteCallBack != NULL)
    {
        rs485TxCompleteCallBack();
        //qInfo("RS 485 TX complete callback called.");
    }

}


void Widget::streamingTimerSlot()
{
    MODBUS_MASTER_RX_TX_S measurement;
    U8 data[2];
    memset(data,0,sizeof(data));


    CHAR  rxdata[100];
    memset(rxdata,0,sizeof(rxdata));

    //set pod type

    measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
    measurement.SlaveAddressU8 = SLAVE_ADDRESS;
    measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_GET_GAS;
    measurement.RegisterCountU16 = sizeof(float)/2;
    measurement.byteCountU8 = 0;

    MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//set pod type

    float currentGas = 0;

    U8 currentGasBytes[4];

    memset(currentGasBytes,0,sizeof(float));

    byteSwap(currentGasBytes,rxdata+3,rxdata[2]);

    memcpy(&currentGas,currentGasBytes,rxdata[2]);

    for (int i = 0; i < seriesLogging->count(); i++)
    {
        QPointF temp = seriesLogging->at(i);

        seriesLogging->replace(i,temp.x()-1,temp.y()); //step time
    }

    //chartLogging->e

    if (currentGas >maxGas)
    {
        maxGas = currentGas;
    }
    seriesLogging->append(0,currentGas);//insert time stamp here



    //chartLogging->createDefaultAxes();



    chartLogging->axisY()->setRange(0, maxGas + 0.25*maxGas);
    chartLogging->axisX()->setRange(-120,0);



    FILE * streamingLogFile = fopen(logFilePath.c_str(),"a");

    //fseek(streamingLogFile,0L,SEEK_END);
    std::string line;

    U64 timeEpoch = QDateTime::currentSecsSinceEpoch();
    line =  QString::number(timeEpoch).toStdString()+","+QString::number(currentGas).toStdString()+"\n";
    fwrite(line.c_str(),sizeof(U8),line.length(),streamingLogFile);



    fclose(streamingLogFile);
}





void Widget::modbusReceiveTimeOutSlot()
{
    //qInfo("Entering modbus receive timeout slot.");

    if (modbusReceiveTimeOutCallBack != NULL)
    {
      modbusReceiveTimeOutCallBack();
    }

}

void Widget::modbusTransmitTimeOutSlot()
{
   // qInfo("Entering modbus transmit timeout slot.");
    if (modbusTransmitTimeOutCallBack != NULL)
    {
        //CHAR buffer[100];

//        if (this->rs485Port.bytesAvailable() > 0)
//        {
//            rs485Read(buffer,rs485Port.bytesAvailable());
//        }

      modbusTransmitTimeOutCallBack();
    }
}

void Widget::heartbeatTimeOutSlot()
{
    //qInfo("Entering HB timeout.");
    //disable all controls for TX

    heartbeatTimer.stop();

    if (streamingTimer.isActive())
    {
        handleBtnStartStreaming();
    }

    ui->btnMeasure->setEnabled(false);

    ui->btnSetPodType->setEnabled(false);
    ui->btnSetSerialNumber->setEnabled(false);

    ui->btnGetPodType->setEnabled(false);
    ui->btnGetSerialNumber->setEnabled(false);

    ui->btnCalibrate->setEnabled(false);
    ui->btnTakeZero->setEnabled(false);
    ui->btnExposeSensor->setEnabled(false);
    ui->btnTakeGas->setEnabled(false);
    ui->btnFinish->setEnabled(false);

    ui->labelCountdown->setText("Waiting for connection...");
    QPalette paletteLCD;

    paletteLCD.setColor(QPalette::WindowText,Qt::red);

    ui->lcdNumberCountdown->setPalette(paletteLCD);

    ui->lcdNumberCountdown->display(ui->spinBoxGasExposureTime->value());


    ui->btnSendFile->setEnabled(false);
    ui->comboModbusFunctionCode->setEnabled(false);
    ui->comboRegisterAddress->setEnabled(false);
    ui->comboRegisterCount->setEnabled(false);
    ui->lineEditModbusData->setEnabled(false);
    ui->spinBoxModbusByteCount->setEnabled(false);

    if (heartbeatTimeOut != NULL)
    {
        heartbeatTimeOut();
    }

    return;
}


void Widget::countdownTimeOutSlot()
{

    if (ui->lcdNumberCountdown->intValue()>0)
    {
        ui->lcdNumberCountdown->display(ui->lcdNumberCountdown->intValue()-1);
    }
    else
    {
        countdownTimer.stop();

        QPalette paletteLCD;

        paletteLCD.setColor(QPalette::WindowText,Qt::green);

        ui->lcdNumberCountdown->setPalette(paletteLCD);



        if (countdownTimer.property((char const *)&TIMER_PROPERTY_STATE).toInt() == TIMER_PROPERTY_STATE_WARMUP)
        {


            ui->btnCalibrate->setEnabled(true);

            ui->lcdNumberCountdown->display(WARMUP_TIME);
            ui->labelCountdown->setText("Sensor Ready! \n Enter calibration mode.");

        }


        if (countdownTimer.property((char const *)&TIMER_PROPERTY_STATE).toInt() == TIMER_PROPERTY_STATE_GAS_EXPOSURE)
        {

            ui->lcdNumberCountdown->display(ui->spinBoxGasExposureTime->value());
            ui->labelCountdown->setText("Sensor ready! \n Take a sample.");
            ui->btnTakeGas->setEnabled(true);

        }


    }


}


//// Register Callbacks:
void Widget::registerIrdaRxCompleteCallBack(void(*function)())
{
    irdaRxCompleteCallBack = function;
}

void registerIrdaRxCompleteCallBackWrapper(void * master, void(*function)())
{
   return reinterpret_cast<Widget*>(master)->registerIrdaRxCompleteCallBack(function);
}

void Widget::registerIrdaTxCompleteCallBack(void(*function)())
{
    irdaTxCompleteCallBack = function;
}

void registerIrdaTxCompleteCallBackWrapper(void * master, void(*function)())
{
   return reinterpret_cast<Widget*>(master)->registerIrdaTxCompleteCallBack(function);
}

void Widget::registerRs485RxCompleteCallBack(void(*function)())
{
    rs485RxCompleteCallBack = function;
}

void registerRs485RxCompleteCallBackWrapper(void * master, void(*function)())
{
   return reinterpret_cast<Widget*>(master)->registerRs485RxCompleteCallBack(function);
}

void Widget::registerRs485TxCompleteCallBack(void(*function)())
{
    rs485TxCompleteCallBack = function;
}

void registerRs485TxCompleteCallBackWrapper(void * master, void(*function)())
{
   return reinterpret_cast<Widget*>(master)->registerRs485TxCompleteCallBack(function);
}

void Widget::registerReceiveTimeOutCallBack(void(*function)())
{
    modbusReceiveTimeOutCallBack = function;
}

void registerReceiveTimeOutCallBackWrapper(void * master, void(*function)())
{
   return reinterpret_cast<Widget*>(master)->registerReceiveTimeOutCallBack(function);
}

void Widget::registerTransmitTimeOutCallBack(void(*function)())
{
    modbusTransmitTimeOutCallBack = function;
}

void registerTransmitTimeOutCallBackWrapper(void * master, void(*function)())
{
    //this.enableTxControls();

   return reinterpret_cast<Widget*>(master)->registerTransmitTimeOutCallBack(function);
}

void Widget::registerHeartbeatTimeOutCallBack(void(*function)())
{
    heartbeatTimeOut = function;
}

void registerHeartbeatTimeOutCallBackWrapper(void * master, void(*function)())
{
    //this.enableTxControls();

   return reinterpret_cast<Widget*>(master)->registerHeartbeatTimeOutCallBack(function);
}



////Additional
void Widget::delay(int msDelay)
{
    //qInfo("Entering delay.");
    QTime dieTime= QTime::currentTime().addMSecs(msDelay);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);


    //qInfo("Exiting delay.");
}

void delayWrapper(void * master, int msDelay)
{
    //qInfo("Calling delay wrapper.");

    return reinterpret_cast<Widget*>(master)->delay(msDelay);
}

U32 Widget::getRs485BaudRate()
{
    return ui->comboRs485->currentText().toInt();
}

U32 getRs485BaudRateWrapper(void * master)
{
    return reinterpret_cast<Widget*>(master)->getRs485BaudRate();
}

void Widget::startModbusReceiveTimer()
{
    modbusReceiveTimer.stop();
    modbusReceiveTimer.start();
  //  qInfo("Call ModbusReceiveTimer started");
}

void startModbusReceiveTimerWrapper(void * master)
{
   // qInfo("Call startModbusReceiveTimerWrapper");
    return reinterpret_cast<Widget*>(master)->startModbusReceiveTimer();
}

void Widget::startModbusTransmitTimer()
{
   // qInfo("Start modbus transmit timer.");
    modbusTransmitTimer.stop();
    modbusTransmitTimer.setInterval(10000);
    modbusTransmitTimer.setSingleShot(true);
    modbusTransmitTimer.start();
  //  qInfo("Modbus transmit timer started.");
}

void startModbusTransmitTimerWrapper(void * master)
{
    return reinterpret_cast<Widget*>(master)->startModbusTransmitTimer();
}


void Widget::startHeartbeatTimer()
{
    heartbeatTimer.start();
}

void startHeartbeatTimerWrapper(void * master)
{
    reinterpret_cast<Widget*>(master)->startHeartbeatTimer();
}

void Widget::handleConncetionActive()
{
    // qInfo("enter hb timber start");


    // qInfo("heartbeat timer is Active");

     ui->btnMeasure->setEnabled(true);


     if (ui->comboFile->currentText().isEmpty())
     {
         ui->btnSendFile->setEnabled(false);
     }
     else
     {
         ui->btnSendFile->setEnabled(true);
     }

     ui->btnSetPodType->setEnabled(true);
     ui->btnSetSerialNumber->setEnabled(true);

     ui->btnGetPodType->setEnabled(true);
     ui->btnGetSerialNumber->setEnabled(true);


     ui->comboModbusFunctionCode->setEnabled(true);
     ui->comboRegisterAddress->setEnabled(true);
     ui->comboRegisterCount->setEnabled(true);
     ui->lineEditModbusData->setEnabled(true);
     ui->spinBoxModbusByteCount->setEnabled(true);

 ///Get Software version
     MODBUS_MASTER_RX_TX_S measurement;
     U8 data[8];
     memset(data,0,sizeof(data));


     CHAR  rxdata[100];
     memset(rxdata,0,sizeof(rxdata));

     //get software version

     measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
     measurement.SlaveAddressU8 = SLAVE_ADDRESS;
     measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_SOFTWARE_VERSION;
     measurement.RegisterCountU16 = sizeof(U32)/2;
     measurement.byteCountU8 = 0;

     //qInfo("Get pod software version.");
     MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//get software version

     QString version;
     int leng = rxdata[2];
     QByteArray versionTemp((char *)(rxdata+3),leng);

     ui->labelSoftwareVersion->setText("Detected Software Version: " +  versionTemp.toHex());


 ///Get last calibrated
 ///

     measurement.FunctionCodeU8 = MODBUS_FUNCTION_READ_HOLDING_REGISTERS;
     measurement.SlaveAddressU8 = SLAVE_ADDRESS;
     measurement.AddressU16 = MODBUS_REGISTER_ADDRESS_CAL_EPOCH;
     measurement.RegisterCountU16 = sizeof(U32)/2;
     measurement.byteCountU8 = 0;

     MODBUS_MASTER_STACK_TxRxR(&measurement,rxdata);//get current M

     U32 epoch = 0;
     U8 epochA[4] = {0,0,0,0};

     byteSwap(epochA,rxdata+3,rxdata[2]);

     memcpy(&epoch,epochA,sizeof(U32));

     QDateTime displayTime;

     displayTime.setTime_t(epoch);



     ui->labelLastCalibrated->setText(displayTime.toString("yyyy-MM-dd hh:mm:ss"));

///
     //qInfo("Start sensor countdown timer.");
     countdownTimer.start();
     ui->labelCountdown->setText("Sensor warming up...");

     //qInfo("Get pod type.");
     handleBtnGetPodType();

     handleBtnGetSerialNumber();

     //qInfo("Start heartbeat timer.");
     heartbeatTimer.start();
}

void handleConncetionActiveWrapper(void * master)
{
    reinterpret_cast<Widget*>(master)->handleConncetionActive();;
}

void Widget::updateFileProgress(U8 progressU8)
{
    ui->progressBarFileTransfer->setValue(progressU8);
}

void updateFileProgressWrapper(void *master, U8 progressU8)
{
    reinterpret_cast<Widget*>(master)->updateFileProgress(progressU8);
}


void Widget::byteSwap(void * destination, void * source, int count)
{
    int up = 0;


    for (int down = count-1; down>=0; down--, up++)
    {
        *((U8*)destination + down) = *((U8*)source + up);//void pointers dont have a defined size. Use U8 to define a byte size
    }
}

