#include "widget.h"
#include <QApplication>



int main(int argc, char *argv[])
{
    QCoreApplication::setApplicationVersion(QString(APP_VERSION));
    QApplication a(argc, argv);
    QThread::currentThread()->setPriority(QThread::HighestPriority);
    Widget w;
    w.show();

    return a.exec();
}
