/******************************************************************************
 * Copyright : � Schauenburg Systems (Pty) Limited, 2009
 * All rights reserved.   No part of this publication may be reproduced, stored
 * in  a retrieval system,  or  transmitted  in  any  form  or  by  any  means,
 * electronic, mechanical, photocopying, recording  or otherwise,  without  the
 * prior written permission of Schauenburg Systems (Pty) Limited.
 * Schauenburg Template Number: TEMPLATE-0002
 * 
 * \file 
 *   Various CRC functions
 *****************************************************************************/
#ifdef __cplusplus
extern "C"{
#endif

#ifndef _CRC_H_
#define _CRC_H_



/******************************************************************************
 * VERSION CONTROL HISTORY
 *****************************************************************************/


/******************************************************************************
 * INCLUDE FILES
 *****************************************************************************/
//OPERATING SYSTEM INCLUDES

//UTILITIES INCLUDES
#include "SCHTypeDefs.h"

//APPLICATION SOFTWARE INCLUDES

//UPPER DRIVER LAYER INCLUDES

//MIDDLE DRIVER LAYER INCLUDES

//HARDWARE LAYER INCLUDES

/******************************************************************************
 * GLOBAL CONSTANTS
 *****************************************************************************/


/******************************************************************************
 * GLOBAL MACROS
 *****************************************************************************/


/******************************************************************************
 * GLOBAL TYPE DEFINITIONS
 *****************************************************************************/



/******************************************************************************
 * GLOBAL VARIABLE DECLARATIONS
 *****************************************************************************/


GLOBAL U16 GetCRCModbus(U8*  u8Message, U8 u8Length);

/*****************************************************************************
 * \fn   CRC_CommsCrc8
 * 
 * \brief
 *    computes an ongoing CRC8 according to the Dallas one wire specification. 
 *  
 * \param      
 *    InBuffer_PU8   - The input buffer on which crc will be calculated.
 *    LengthU16      - Length of the data to be included in the calc.  
 *
 * \return           - the crc8 value
 *****************************************************************************/
GLOBAL U8 CRC_CommsCrc8( U8 *InBuffer_PU8, U16 LengthU16 );


/*****************************************************************************
 * \fn   CRC_ComputeCrc16
 * 
 * \brief
 *    computes an ongoing CRC16
 *  
 * \param      
 *    InBuffer_PU8   - The input buffer on which crc will be calculated.
 *    LengthU16      - Length of the data to be included in the calc.  
 *
 * \return           - the crc16 value
 *****************************************************************************/
GLOBAL U16 CRC_ComputeCrc16( U8 *InBuffer_PU8, U16 LengthU16 );


 /*****************************************************************************
 * \fn   CRC_ComputeCrc16
 * 
 * \brief
 *    computes an ongoing CRC16 using the CCITT table
 *  
 * \param      
 *    InBuffer_PU8   - The input buffer on which crc will be calculated.
 *    LengthU16      - Length of the data to be included in the calc.  
 *
 * \return           - the crc16 value
 *****************************************************************************/
GLOBAL U16 CRC_ComputeCrc16CcittU16( U8 *InBuffer_PU8, U16 LengthU16 );
 /*****************************************************************************
 * \fn   CRC_ComputeCrc16
 * 
 * \brief
 *    computes an ongoing CRC16 using the CCITT table
 *  
 * \param      
 *    InBuffer_PU8   - The input buffer on which crc will be calculated.
 *   oldCRC       - CRC to continue with
 *    LengthU16      - Length of the data to be included in the calc.  
 *
 * \return           - the crc16 value
 *****************************************************************************/
GLOBAL U16 CRC_ComputeCrc16CcittOngoingU16( U8 *InBuffer_PU8, U16 oldCRC, U16 LengthU16 );

/*****************************************************************************
 * \fn   CRC_ComputeCrc16Ccitt8408U16
 * 
 * \brief
 *    computes a CRC16 using polynomial of 0x8408
 *    Taken from PTrack, left as is...
 *  
 * \param      
 *    InBuffer_PU8   - The input buffer on which crc will be calculated.
 *    LengthU16      - Length of the data to be included in the calc.  
 *
 * \return           - the crc16 value
 *****************************************************************************/
GLOBAL U16 CRC_ComputeCrc16Ccitt8408U16(U8 *BufferP, U16 lengthU16);

#endif

#ifdef __cplusplus
}
#endif
/* ***************************** end/einde/fin ***************************** */
