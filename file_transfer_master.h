/******************************************************************************
 * Copyright : � Schauenburg Systems (Pty) Limited, 2011
 * All rights reserved.   No part of this publication may be reproduced, stored
 * in  a retrieval system,  or  transmitted  in  any  form  or  by  any  means,
 * electronic, mechanical, photocopying, recording  or otherwise,  without  the
 * prior written permission of Schauenburg Systems (Pty) Limited.
 * FILE_TRANSFER_MASTER Number: FILE_TRANSFER_MASTER-0002
 *****************************************************************************/

/**************************************************************************//**
 * \file		FILE_TRANSFER_MASTER???.???
 * \brief	Give a short description of the primary function of this file here.
 *****************************************************************************/

#ifdef __cplusplus
extern "C"{
#endif




#ifndef _FILE_TRANSFER_MASTER_H_
#define _FILE_TRANSFER_MASTER_H_


/******************************************************************************
 * VERSION CONTROL HISTORY
 *****************************************************************************/
/*
 * $Log$
 */


/******************************************************************************
 * INCLUDE FILES
 *****************************************************************************/
//OPERATING SYSTEM INCLUDES

//UTILITIES INCLUDES
//#include "project_conf.h"
#include "SCHTypeDefs.h"
#include "serror.h"
#include <stdio.h>
#include <stdlib.h>

//APPLICATION SOFTWARE INCLUDES

//UPPER DRIVER LAYER INCLUDES

//MIDDLE DRIVER LAYER INCLUDES


//HARDWARE LAYER INCLUDES



/******************************************************************************
 * GLOBAL CONSTANTS
 *****************************************************************************/


/******************************************************************************
 * GLOBAL MACROS
 *****************************************************************************/


/******************************************************************************
 * GLOBAL TYPE DEFINITIONS
 *****************************************************************************/

#pragma pack(1)
typedef struct fileXferDefinitionT
{
	U16			typeU16;				// enumerated types
	U32			lengthU32;				// file length in bytes
	U32			crcU32;				// 32-bit CRC
	U32			file_versionU32;		// identifier for file version
	U16			packet_countU16;		// no. of file fragments
	U8			packet_sizeU8;		// size of fragments, excluding payloader header size
	U8			last_packet_sizeU8;	// size of the last tx fragments
	U8			master_session_idU8;	// identifies transfer session
	U8			timeoutU8;			// defines session time(complete file transfer) to live in seconds
} fileXferDefinitionS;
#pragma pack()

#pragma pack(1)
typedef struct fileXferPayloadT
{
	U8		masterSessionIdU8;
	U8		lengthU8;
	U16		packetNoU16;
}fileXferPayloadS;
#pragma pack()

#pragma pack(1)
typedef struct sessionInfoT
{
U32            	startTimer;         //transfer start indicator
U16            	nextPacket;         //next expexted data packet number
U8             	localSessionId;     //session assigned for this side
BOOL            busyToXfer;         //set while busy transferring, clear if last packer received
BOOL            calcCrcValid;       //crc at end of transfer was valid or not
CHAR            fname[8];              //file system name to use - new
}sessionInfoS;
#pragma pack()




/******************************************************************************
 * GLOBAL VARIABLE DECLARATIONS
 *****************************************************************************/


/******************************************************************************
 * GLOBAL FUNCTIONS : STANDARD FUNCTIONS
 *****************************************************************************/


/******************************************************************************
 * GLOBAL FUNCTIONS : ADDITIONAL FUNCTIONS
 *****************************************************************************/

SERROR FILE_TRANSFER_start(CHAR* fileNamePC,U8 podModbusAdressU8,U8 fileTypeU8,void * master);
SERROR FILE_TRANSFER_send(U8 sessionIdU8, U8 podModbusAddressU8);
SERROR FILE_TRANSFER_getSessionInfo(U8 sessionIdU8, U8 podModbusAddressU8);



/*****************************************************************************
 * \brief
 * Provides the XML interface for this module.
 * 
 * \param		attrib			attribute string
 * \param		value_in			value-in string
 * \param		value_out		value-out string
 * \param		value_maxlen	max length for value-out string
 * \return		none				
 *****************************************************************************/


#endif	//end #ifndef _FILE_TRANSFER_MASTER_H_

#ifdef __cplusplus
}
#endif

/******************************************************************************
 * END
 *****************************************************************************/
