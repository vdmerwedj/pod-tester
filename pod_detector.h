/******************************************************************************
 * Copyright : � Schauenburg Systems (Pty) Limited, 2011
 * All rights reserved.   No part of this publication may be reproduced, stored
 * in  a retrieval system,  or  transmitted  in  any  form  or  by  any  means,
 * electronic, mechanical, photocopying, recording  or otherwise,  without  the
 * prior written permission of Schauenburg Systems (Pty) Limited.
 * Template Number: POD_DETECTOR-0002
 *****************************************************************************/
#ifdef __cplusplus
extern "C"{
#endif
/**************************************************************************//**
 * \file		POD_DETECTOR???.???
 * \brief	Give a short description of the primary function of this file here.
 *****************************************************************************/
#ifndef _POD_DETECTOR_H_
#define _POD_DETECTOR_H_


/******************************************************************************
 * VERSION CONTROL HISTORY
 *****************************************************************************/
/*
 * $Log$
 */


/******************************************************************************
 * INCLUDE FILES
 *****************************************************************************/
//OPERATING SYSTEM INCLUDES

//UTILITIES INCLUDES
//#include "project_conf.h"
#include "SCHTypeDefs.h"
#include "serror.h"

//APPLICATION SOFTWARE INCLUDES

//UPPER DRIVER LAYER INCLUDES

//MIDDLE DRIVER LAYER INCLUDES

//HARDWARE LAYER INCLUDES


/******************************************************************************
 * GLOBAL CONSTANTS
 *****************************************************************************/


/******************************************************************************
 * GLOBAL MACROS
 *****************************************************************************/
#define POD_DETECTOR_MACRO(_x,_y)		(_x + _y)


/******************************************************************************
 * GLOBAL TYPE DEFINITIONS
 *****************************************************************************/

/**
 * Defines the specific sleep modes for this module.
 */
typedef enum POD_DETECTOR_SLEEP_E
{
	POD_DETECTOR_SLEEP_SLOW,			///< some description
	POD_DETECTOR_SLEEP_1,				///< some description
	POD_DETECTOR_SLEEP_2,				///< some description
	POD_DETECTOR_SLEEP_FULL,			///< some description
} POD_DETECTOR_SLEEP_E;

/**
 * This structure represent some entity.
 */
typedef struct POD_DETECTOR_TYPE_T
{
	F32		ValF32;			///< floating-point value
	U32		ValU32;			///< unsigned int-32 value
} POD_DETECTOR_TYPE_S;

#pragma pack(1)
typedef struct ModbusSetupT{
    U32 BaudrateU32;
    U8 ModbusAddressU8;
    U16 CRC;
} ModbusSetupS;
#pragma pack()
/******************************************************************************
 * GLOBAL VARIABLE DECLARATIONS
 *****************************************************************************/
GLOBAL BOOL POD_DETECTOR_VariableB;


/******************************************************************************
 * GLOBAL FUNCTIONS : STANDARD FUNCTIONS
 *****************************************************************************/
GLOBAL SERROR POD_DETECTOR_InitR(void * irdaThreadPV);
GLOBAL SERROR POD_DETECTOR_ExitR(VOID);
GLOBAL SERROR POD_DETECTOR_SleepR(POD_DETECTOR_SLEEP_E mode);
GLOBAL SERROR POD_DETECTOR_WakeupR(VOID);
GLOBAL SERROR POD_DETECTOR_SelftestR(VOID);
GLOBAL SERROR POD_DETECTOR_XmlR(CHAR* attribCP, CHAR* value_inCP, CHAR* value_outCP, U16 value_maxlenU16);


/******************************************************************************
 * GLOBAL FUNCTIONS : ADDITIONAL FUNCTIONS
 *****************************************************************************/

/*****************************************************************************
 * \brief
 * Provides the XML interface for this module.
 * 
 * \param		attrib			attribute string
 * \param		value_in			value-in string
 * \param		value_out		value-out string
 * \param		value_maxlen	max length for value-out string
 * \return		none				
 *****************************************************************************/
GLOBAL U8 POD_DETECTOR_AGlobalFuncU8(CHAR arg1C);


#endif	//end #ifndef _POD_DETECTOR_H_

#ifdef __cplusplus
}
#endif
/******************************************************************************
 * END
 *****************************************************************************/
