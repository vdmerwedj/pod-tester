/******************************************************************************
 * Copyright : � Schauenburg (Pty) Limited, 2016
 * All rights reserved.   No part of this publication may be reproduced, stored
 * in  a retrieval system,  or  transmitted  in  any  form  or  by  any  means,
 * electronic, mechanical, photocopying, recording  or otherwise,  without  the
 * prior written permission of Schauenburg (Pty) Limited.
 * Template Number: POD_DETECTOR-0001
 *****************************************************************************/
#ifdef __cplusplus
extern "C"{
#endif
/**************************************************************************//**
 * \file		POD_DETECTOR???.???
 * \brief	Give a short description of the primary function of this file here.
 *****************************************************************************/
#ifndef _POD_DETECTOR_C_
#define _POD_DETECTOR_C_
#define THIS_MODULE				MODULE_POD_DETECTOR


/******************************************************************************
 * VERSION CONTROL HISTORY
 *****************************************************************************/
/*
 * $Log$
 */


/******************************************************************************
 * INCLUDE FILES
 *****************************************************************************/
//OPERATING SYSTEM INCLUDES
//#include "cmsis_os.h"


//UTILITIES INCLUDES
//#include "project_conf.h"
#include "SCHTypeDefs.h"
#include "serror.h"
#include "crc.h"
//#include "module.h"
#include "mbcrc.h"
//#include "Uptime.h"
#include "widget.h"



////APPLICATION SOFTWARE INCLUDES
//
////UPPER DRIVER LAYER INCLUDES
//
////MIDDLE DRIVER LAYER INCLUDES
//
////HARDWARE LAYER INCLUDES
//#include "scpu_conf.h"
//
////PRIVATE + CONF HEADER FILE(S)
#include "pod_detector.h"
//#include "api_conf.h"
//#include "pod_configurator.h"
//#include "pod_scanner.h"

//#include "template_conf.h"


/******************************************************************************
 * MODULE CONSTANTS
 *****************************************************************************/
#define MY_CONSTANT				(30)u					///< Description for my constant
#define POD_HELLO 			0xAA
#define POD_ACKNOWLEDGE 	0xBB



/******************************************************************************
 * MODULE MACROS
 *****************************************************************************/
#define UART_CONFIG_OFFSET 					2

/******************************************************************************
 * MODULE TYPE DEFINITIONS
 *****************************************************************************/
typedef U8					POD_DETECTOR_HandleT;			///< some description


/******************************************************************************
 * GLOBAL VARIABLES
 *****************************************************************************/
BOOL POD_DETECTOR_VariableB = FALSE;								///< some description


/******************************************************************************
 * MODULE VARIABLES
 *****************************************************************************/
LOCAL U8		templateCntU8;						///< some description


typedef struct HeartBeatT{
    U8 PortU8;
	U8 RxMessageU8;
	U8 IgnoreCountU8;
    void * irdaThreadPV;
//	TimerHandle_t	  TimeOutT;
    void*	  TimeOutT;
	BOOL Acknowledged;
//	SemaphoreHandle_t HelloSM;
//	SemaphoreHandle_t AcknowledgeSM;
//	SemaphoreHandle_t BeatSM;
} HeartBeatS;

LOCAL HeartBeatS irda1HeartBeatS = {0,0,0,NULL,NULL,FALSE};

//LOCAL HeartBeatS irda2HeartBeatS = {0,0,0,NULL,FALSE};

//LOCAL HeartBeatS irda3HeartBeatS = {0,0,0,NULL,FALSE};

//LOCAL HeartBeatS irda4HeartBeatS = {0,0,0,NULL,FALSE};

//LOCAL HeartBeatS irda5HeartBeatS = {0,0,0,NULL,FALSE};








/******************************************************************************
 * LOCAL FUNCTIONS PROTOTYPES
 *****************************************************************************/
LOCAL BOOL template_funcB(VOID);
LOCAL void RxCompleteIrda1();//moved to global
LOCAL void RxCompleteIrda2();
LOCAL void RxCompleteIrda3();
LOCAL void RxCompleteIrda4();
LOCAL void RxCompleteIrda5();


LOCAL SERROR SetupPortModbus(U8 PortU8);
LOCAL void IrqHandler(HeartBeatS * portHearBeatS);
//LOCAL void vTimerCallBack (TimerHandle_t xTimer);
LOCAL void hearbeatTimerCallBack();
LOCAL void InitializeHeartBeatStruct(HeartBeatS * PortHeartBeatS, U8 PortU8, void * irdaThreadPV);


/******************************************************************************
 * GLOBAL FUNCTIONS : STANDARD FUNCTIONS
 *****************************************************************************/

/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR POD_DETECTOR_InitR(void* irdaThreadPV)
{



    InitializeHeartBeatStruct(&irda1HeartBeatS,3,irdaThreadPV);

//	InitializeHeartBeatStruct(&irda2HeartBeatS,UART_PORT_IRDA2);
//	InitializeHeartBeatStruct(&irda3HeartBeatS,UART_PORT_IRDA3);
//	InitializeHeartBeatStruct(&irda4HeartBeatS,UART_PORT_IRDA4);
//	InitializeHeartBeatStruct(&irda5HeartBeatS,UART_PORT_IRDA5);

	//register callbacks
    registerIrdaRxCompleteCallBackWrapper(irdaThreadPV,RxCompleteIrda1);
    registerHeartbeatTimeOutCallBackWrapper(irdaThreadPV,hearbeatTimerCallBack);

//	API_UART_RegisterRxCpltCallback(UART_PORT_IRDA2,RxCompleteIrda2);
//	API_UART_RegisterRxCpltCallback(UART_PORT_IRDA3,RxCompleteIrda3);
//	API_UART_RegisterRxCpltCallback(UART_PORT_IRDA4,RxCompleteIrda4);
//	API_UART_RegisterRxCpltCallback(UART_PORT_IRDA5,RxCompleteIrda5);

	//clear Uart Buffers for IRDA
//	STM32F4S_UART_ClearRxBuffer(UART_PORT_IRDA1);
//	STM32F4S_UART_ClearRxBuffer(UART_PORT_IRDA2);
//	STM32F4S_UART_ClearRxBuffer(UART_PORT_IRDA3);
//	STM32F4S_UART_ClearRxBuffer(UART_PORT_IRDA4);
//	STM32F4S_UART_ClearRxBuffer(UART_PORT_IRDA5);

	return(SERROR_NONE);
}	//end POD_DETECTOR_InitR()


/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR POD_DETECTOR_ExitR(VOID)
{
	//something
	return(SERROR_NONE);
}	//end POD_DETECTOR_ExitR()


/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR POD_DETECTOR_SleepR(POD_DETECTOR_SLEEP_E mode)
{
	//something
	return(SERROR_NONE);
}	//end POD_DETECTOR_SleepR()


/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR POD_DETECTOR_WakeupR(VOID)
{
	//something
	return(SERROR_NONE);
}	//end POD_DETECTOR_WakeupR()


/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR POD_DETECTOR_SelftestR(VOID)
{
	#if PROJECT_COMPILE_SELFTEST
	//something
	#endif	//PROJECT_COMPILE_SELFTEST
	return(SERROR_NONE);
}	//end POD_DETECTOR_SelftestR()


/******************************************************************************
 * See header file for details
 *****************************************************************************/
SERROR POD_DETECTOR_XmlR(CHAR* attribCP, CHAR* value_inCP, CHAR* value_outCP, U16 value_maxlenU16)
{
	return(SERROR_NONE);
}	//end POD_DETECTOR_XmlR()


/******************************************************************************
 * GLOBAL FUNCTIONS : ADDITIONAL FUNCTIONS
 *****************************************************************************/

/******************************************************************************
 * See header file for details
 *****************************************************************************/
U8 POD_DETECTOR_AGlobalFuncU8(CHAR arg1C)
{
	return(1);
}	//end POD_DETECTOR_GlobalFuncU8()


/******************************************************************************
 * LOCAL FUNCTIONS
 *****************************************************************************/

/******************************************************************************
 * \brief
 * This is the brief description for the function.
 *
 * \param		none
 * \return							Some description of what the return value means.
 * \retval		SERROR_NONE			no errors occurred
 * \retval		SERROR_?			one or more errors
 *
 * \todo
 * Some future functionality.
 *****************************************************************************/
LOCAL SERROR local_funcB(VOID)
{
	return(FALSE);
}	//end local_funcB()

LOCAL void IrqHandler(HeartBeatS * portHeartBeatS)
{

//	PodScannerPodS PodScannerS;
//	PodS NewPodS;
//	static signed long xHigherPriorityTaskWoken;
	U8 rxCountU8 =  1;
//	xHigherPriorityTaskWoken = pdFALSE;

//	API_UART_RxDataR(portHeartBeatS->PortU8,&portHeartBeatS->RxMessageU8,NULL,rxCountU8);
    irdaReadWrapper(portHeartBeatS->irdaThreadPV,&portHeartBeatS->RxMessageU8,rxCountU8);

	switch (portHeartBeatS->RxMessageU8)
	{
		case POD_HELLO:
			//do setup
//			if (portHeartBeatS->IgnoreCountU8 == 0 && portHeartBeatS->Acknowledged != TRUE )
//			{

                //xTimerStartFromISR(portHeartBeatS->TimeOutT,xHigherPriorityTaskWoken);
                SetupPortModbus(portHeartBeatS->PortU8);
                delayWrapper(portHeartBeatS->irdaThreadPV,1);
//			}
//			else if (portHeartBeatS->IgnoreCountU8 == 0 && portHeartBeatS->Acknowledged == TRUE)
//			{

                //xTimerStopFromISR(portHeartBeatS->TimeOutT,xHigherPriorityTaskWoken);

				//remove pod
//				PodScannerS.ModbusAddressU8 = portHeartBeatS->PortU8 -UART_CONFIG_OFFSET;
//				POD_SCANNER_RemovePod(&PodScannerS);
//				portHeartBeatS->IgnoreCountU8 = 3 - 1; //already ignoring it the first time here
//				portHeartBeatS->Acknowledged = FALSE;
//			}
//			else if (portHeartBeatS->IgnoreCountU8 > 0 && portHeartBeatS->Acknowledged != TRUE)
//			{
//				portHeartBeatS->IgnoreCountU8--;
//			}
		break;

		case POD_ACKNOWLEDGE:

//			NewPodS.ModbusAddressU8 = portHeartBeatS->PortU8 - UART_CONFIG_OFFSET;
//			NewPodS.PodTypeU8 = 0;
//			POD_CONFIGURATOR_PushRequestQueue(&NewPodS);
            portHeartBeatS->Acknowledged = TRUE;
            handleConncetionActiveWrapper(portHeartBeatS->irdaThreadPV);



		break;

		default:
			if (portHeartBeatS->Acknowledged == TRUE)
			{
                //xTimerResetFromISR(portHeartBeatS->TimeOutT,xHigherPriorityTaskWoken);

				//might put in a delay here
                //API_UART_TxDataR(portHeartBeatS->PortU8,(CHAR *)&(portHeartBeatS->RxMessageU8),sizeof(portHeartBeatS->RxMessageU8));
                irdaWriteWrapper(portHeartBeatS->irdaThreadPV,(CHAR *)&(portHeartBeatS->RxMessageU8),sizeof(U8));


                startHeartbeatTimerWrapper(portHeartBeatS->irdaThreadPV);



            }
		break;
	}

//	if (xHigherPriorityTaskWoken != pdFALSE)
//	{
//		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
//	}
}


void RxCompleteIrda1()

{
    irda1HeartBeatS.PortU8 = 3;//could remove
    IrqHandler(&irda1HeartBeatS);
}

//LOCAL void RxCompleteIrda2()
//{
//	IrqHandler(&irda2HeartBeatS);
//}

//LOCAL void RxCompleteIrda3()
//{
//	IrqHandler(&irda3HeartBeatS);
//}

//LOCAL void RxCompleteIrda4()
//{
//	IrqHandler(&irda4HeartBeatS);
//}

//LOCAL void RxCompleteIrda5()
//{
//	IrqHandler(&irda5HeartBeatS);
//}


LOCAL SERROR SetupPortModbus(U8 PortU8)
{

	U8 HelloC = 0xAA;

	ModbusSetupS PodModbusSetupS;

	//might put in a delay here

    //API_UART_TxDataR(PortU8,(CHAR * )&HelloC,sizeof(HelloC)); //right
    irdaWriteWrapper(irda1HeartBeatS.irdaThreadPV,(CHAR * )&HelloC,sizeof(HelloC));


    PodModbusSetupS.BaudrateU32 = getRs485BaudRateWrapper(irda1HeartBeatS.irdaThreadPV);
    PodModbusSetupS.ModbusAddressU8 = PortU8-UART_CONFIG_OFFSET;//for this application just hardcode to 1?
	PodModbusSetupS.CRC = 0;

    PodModbusSetupS.CRC = usMBCRC16((U8*)&PodModbusSetupS,(U16)(sizeof(ModbusSetupS)-sizeof(PodModbusSetupS.CRC)));

	//might put in a delay here

	//setup modbus comms

    //API_UART_TxDataR(PortU8,(CHAR*)&PodModbusSetupS,sizeof(ModbusSetupS));
    irdaWriteWrapper(irda1HeartBeatS.irdaThreadPV,(CHAR*)&PodModbusSetupS,sizeof(ModbusSetupS));
    return(SERROR_NONE);
}



void hearbeatTimerCallBack()
{
    irda1HeartBeatS.Acknowledged = FALSE;
}

//void vTimerCallBack (TimerHandle_t xTimer)
//{

//	PodScannerPodS PodS;

//	//HeartBeatS * TempS = ((HeartBeatS *)pvTimerGetTimerID(xTimer));
//	//TempS->Acknowledged = FALSE;

//	((HeartBeatS *)pvTimerGetTimerID(xTimer))->Acknowledged = FALSE;


//	PodS.ModbusAddressU8 = ((HeartBeatS *)pvTimerGetTimerID(xTimer))->PortU8 -UART_CONFIG_OFFSET;

//	POD_SCANNER_RemovePod(&PodS);
////send message to pod scanner

//}

void InitializeHeartBeatStruct(HeartBeatS * PortHeartBeatS, U8 PortU8,void * irdaThreadPV)
{
	PortHeartBeatS->PortU8 = PortU8;
	PortHeartBeatS->RxMessageU8 = 0;
	PortHeartBeatS->IgnoreCountU8 = 0;
    //PortHeartBeatS->TimeOutT = xTimerCreate(NULL, pdMS_TO_TICKS(2000),pdFALSE,PortHeartBeatS,vTimerCallBack);
	PortHeartBeatS->Acknowledged = FALSE;
    PortHeartBeatS->irdaThreadPV = irdaThreadPV;

}


#endif	//#ifndef _POD_DETECTOR_C_

#ifdef __cplusplus
}
#endif
/******************************************************************************
 * END
 *****************************************************************************/
