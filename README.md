# README #



### What is this repository for? ###

This repo contains the first version of the Pod Tester 3000 application.

### How do I get set up? ###

The setup is simple. Just install QT (The open source version) at "https://www1.qt.io/download-open-source/".

Download or clone this repository.

Once this has been done open Qt Creator and while on the Welcome Page select "Open Project". 
Go to the project and select the file named "pod_tester.pro".

A deployed version of Pod Tester 3000 is also availible in this repository at: https://bitbucket.org/vdmerwedj/pod-tester/downloads/Pod%20Tester%20V1.0.0.0.zip

For a guide on what files to include in a delployment go check out this page: https://wiki.qt.io/Deploy_an_Application_on_Windows
