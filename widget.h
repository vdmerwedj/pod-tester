#ifndef WIDGET_H
#define WIDGET_H

#ifdef __cplusplus
#include <QMutex>
#include <QWidget>
#include <QThread>
#include <QPushButton>
#include <QtSerialPort/QSerialPort>
#include <QTimer>
#include <QErrorMessage>
#include <QtCharts/QChartView>
#include <QtCharts/QSplineSeries>


extern "C"
{
#include "SCHTypeDefs.h"
#include "modbus_master_stack.h"
}

QT_CHARTS_USE_NAMESPACE
namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void registerIrdaRxCompleteCallBack(void(*function)());
    void registerIrdaTxCompleteCallBack(void(*function)());
    void registerRs485RxCompleteCallBack(void(*function)());
    void registerRs485TxCompleteCallBack(void(*function)());
    void registerReceiveTimeOutCallBack(void(*function)());
    void registerTransmitTimeOutCallBack(void(*function)());
    void registerHeartbeatTimeOutCallBack(void(*function)());
    void updateFileProgress(U8 progressU8);

    void delay(int msDelay);


    void irdaRead(CHAR * rxPC,U8 lengthU8);
    void irdaWrite(CHAR * txPC, U8 lengthU8);
    void rs485Read(CHAR * rxPC,U8 lengthU8);
    void rs485Write(CHAR * txPC, U8 lengthU8);
    int  rs485GetByteAvailable();


    U32 getRs485BaudRate();

    void startModbusReceiveTimer();
    void startModbusTransmitTimer();
    void startHeartbeatTimer();

    void handleConncetionActive();

    QTimer heartbeatTimer;

     QSerialPort rs485Port;



private:

    void byteSwap(void * destination, void * source, int count);
    typedef void(*callBackFunctionPointer)();
    callBackFunctionPointer irdaTxCompleteCallBack;
    callBackFunctionPointer irdaRxCompleteCallBack;
    callBackFunctionPointer rs485TxCompleteCallBack;
    callBackFunctionPointer rs485RxCompleteCallBack;
    callBackFunctionPointer modbusReceiveTimeOutCallBack;
    callBackFunctionPointer modbusTransmitTimeOutCallBack;
    callBackFunctionPointer heartbeatTimeOut;
    void enableTxControls();




    QChart *chartLogging;
    QSplineSeries *seriesLogging;
    double maxGas = 0;
    U32 loggingStartTime = 0;
    std::string logFilePath;







 signals:


private slots:

    void handleBtnSelectLogFile();

    void handleBtnConnect();
    void handleBtnMeasure();
    void handleBtnCalibrate();
    void handleBtnSelectFile();
    void handleBtnSendFile();
    void handleBtnSetPodType();
    void handleBtnGetPodType();
    void handleBtnSetSerialNumber();
    void handleBtnGetSerialNumber();
    void handleBtnStartStreaming();


    void refreshComboBoxRs485ComPort();
    void refreshComboBoxIrdaComPort();

    void handleBtnTakeZero();
    void handleBtnExposeSensor();
    void handleBtnTakeGas();
    void handleBtnFinish();


    void handleComboFileChanged();

    void handleLineEditModbusData();


    void streamingTimerSlot();
    void modbusReceiveTimeOutSlot();
    void modbusTransmitTimeOutSlot();
    void countdownTimeOutSlot();

    void heartbeatTimeOutSlot();

    void irdaRxComplete();
    void rs485RxComplete();
    void irdaTxComplete();
    void rs485TxComplete();

private:


    QTimer streamingTimer;
    QTimer modbusReceiveTimer;
    QTimer modbusTransmitTimer;
    QTimer countdownTimer;

    Ui::Widget *ui;
    QSerialPort irdaPort;


    QErrorMessage widgetError;



protected:
    bool eventFilter(QObject* o, QEvent* e);





};
#endif

#ifdef __cplusplus
extern "C"
{
#endif
void irdaReadWrapper(void * master, CHAR * rxPC, U8 lengthU8);
void irdaWriteWrapper(void * master, CHAR * txPC, U8 lengthU8);
void rs485ReadWrapper(void * master, CHAR * rxPC, U8 lengthU8);
void rs485WriteWrapper(void * master, CHAR * txPC, U8 lengthU8);
U32 getRs485BaudRateWrapper(void * master);

void startModbusReceiveTimerWrapper(void * master);
void startModbusTransmitTimerWrapper(void * master);
void startHeartbeatTimerWrapper(void * master);

void handleConncetionActiveWrapper(void * master);


void delayWrapper(void * master, int msDelay);
void registerIrdaRxCompleteCallBackWrapper(void * master, void(*function)());
void registerIrdaTxCompleteCallBackWrapper(void * master, void(*function)());
void registerRs485RxCompleteCallBackWrapper(void * master, void(*function)());
void registerRs485TxCompleteCallBackWrapper(void * master, void(*function)());
int rs485GetByteAvailableWrapper(void * master);
void registerReceiveTimeOutCallBackWrapper(void * master, void(*function)());
void registerTransmitTimeOutCallBackWrapper(void * master, void(*function)());
void registerHeartbeatTimeOutCallBackWrapper(void * master, void(*function)());

void updateFileProgressWrapper(void * master, U8 progressU8);

#ifdef __cplusplus
}
#endif

#endif // WIDGET_H
